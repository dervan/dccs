from unittest import TestCase

import numpy as np
from numpy.testing import assert_array_equal, assert_array_almost_equal
from pyrr import Matrix33

import dccs.math.algebra as algebra


class AlgebraTests(TestCase):

    def test_cross_vec(self):
        v = np.array([3, 4, 5])
        v2 = np.array([7, 9, 11])
        mtx = algebra.cross_vec_to_mtx(v)
        assert_array_almost_equal(np.cross(v, v2), mtx.dot(v2))

    def test_cross_vec_inverse(self):
        v = np.array([3, 4, 5])
        mtx = algebra.cross_vec_to_mtx(v)
        v2 = algebra.cross_mtx_to_vec(mtx)
        assert_array_equal(v, v2)

    def test_rotation_angles(self):
        angles = [0.2, 0.8, 0.7]
        mtx = algebra.rotation_angles_to_mtx(angles)
        angles2 = algebra.rotation_mtx_to_angles(mtx)
        mtx2 = algebra.rotation_angles_to_mtx(angles2)
        assert_array_almost_equal(angles, angles2, 4)
        assert_array_almost_equal(mtx, mtx2, 4)

    def test_rotation_angles_negative(self):
        angles = [-0.2, -0.8, 0.7]
        mtx = algebra.rotation_angles_to_mtx(angles)
        angles2 = algebra.rotation_mtx_to_angles(mtx)
        mtx2 = algebra.rotation_angles_to_mtx(angles2)
        assert_array_almost_equal(angles, angles2, 4)
        assert_array_almost_equal(mtx, mtx2, 4)

    def test_rotation_angles_big(self):
        angles = [1.5, 1.2, 1.45]
        mtx = algebra.rotation_angles_to_mtx(angles)
        angles2 = algebra.rotation_mtx_to_angles(mtx)
        mtx2 = algebra.rotation_angles_to_mtx(angles2)
        assert_array_almost_equal(angles, angles2, 4)
        assert_array_almost_equal(mtx, mtx2, 4)

    def test_external_correctness(self):
        rx = Matrix33.from_x_rotation(0.5)
        ry = Matrix33.from_y_rotation(0.7)
        rz = Matrix33.from_z_rotation(0.9)
        m33_mtx = rz.dot(ry).dot(rx)
        mtx = algebra.rotation_angles_to_mtx([0.5, 0.7, 0.9])
        assert_array_almost_equal(np.array(m33_mtx), mtx, 4)

    def test_1000_random_inverse(self):
        for i in range(1, 1000):
            angles = np.random.rand(3) * 2 * np.pi - np.pi
            mtx = algebra.rotation_angles_to_mtx(angles)
            angles2 = algebra.rotation_mtx_to_angles(mtx)
            angles3 = algebra.rotation_mtx_to_angles(mtx, True)
            try:
                assert_array_almost_equal(angles, angles2, 4)
            except AssertionError:
                assert_array_almost_equal(angles, angles3, 4)
                angles2 = angles3

            mtx2 = algebra.rotation_angles_to_mtx(angles2)
            assert_array_almost_equal(mtx, mtx2, 4)


class MovementMatrixTests(TestCase):
    def test_full_movement(self):
        for i in range(100):
            t = np.random.randn(3) * 20
            angles = np.random.rand(3) * 2 * np.pi - np.pi
            t1 = algebra.full_transformation_mtx(t, angles)
            r_mtx = algebra.rotation_angles_to_mtx(angles)
            r_inv = np.linalg.inv(r_mtx)
            inv_angles = algebra.rotation_mtx_to_angles(r_inv)
            t2 = algebra.full_transformation_mtx(r_mtx.dot(-t), inv_angles)

            assert_array_almost_equal(t2.dot(t1), np.eye(4))


class QuaternionAnglesTests(TestCase):
    def test_quat_to_angle(self):
        for i in range(100):
            start_angles = (np.random.rand(3) * 2 - 1) * np.pi
            q = algebra.angles_to_quaternion(start_angles)
            result_angles = algebra.quaternion_to_angles(q)
            result_angles2 = algebra.quaternion_to_angles(q, True)
            close_to_1 = np.allclose(start_angles, result_angles)
            close_to_2 = np.allclose(start_angles, result_angles2)
            self.assertTrue(close_to_1 or close_to_2,
                            "Neither of {} and {} are close to {}".format(result_angles, result_angles2, start_angles))
