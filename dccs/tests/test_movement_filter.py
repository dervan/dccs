from unittest import TestCase

import numpy as np
from pyrr import Quaternion

from dccs.math.filters import Movement3dModel, Movement3dModelBis, QuaternionFilter, Movement3dModelQuat
from numpy.testing import assert_allclose
from dccs.math.algebra import angles_to_quaternion, rotation_angles_to_mtx


class QuaternionTest(TestCase):
    def setUp(self):
        self.tested = QuaternionFilter()
        np.set_printoptions(precision=4, linewidth=180, suppress=True)

    def test_model_predict_const(self):
        start_ang = np.random.rand(3) * 2 * np.pi - np.pi
        state = np.zeros(7)
        state[0:4] = angles_to_quaternion(start_ang)
        predicted = self.tested.process_state(state)
        assert_allclose(angles_to_quaternion(start_ang), predicted[0:4])
        assert_allclose(np.zeros(3), predicted[4:])

    def test_model_predict(self):
        start_ang = np.random.rand(3) * 2 * np.pi - np.pi
        diff_ang = np.random.rand(3) * 2 * np.pi - np.pi
        state = np.zeros(7)
        state[0:4] = angles_to_quaternion(start_ang)
        state[4:] = diff_ang
        predicted = self.tested.process_state(state)
        r_mtx = rotation_angles_to_mtx(diff_ang).dot(rotation_angles_to_mtx(start_ang))
        assert_allclose(diff_ang, predicted[4:])
        try:
            assert_allclose(Quaternion.from_matrix(r_mtx), predicted[0:4])
        except AssertionError:
            assert_allclose(Quaternion.from_matrix(r_mtx), -predicted[0:4])

    def test_covariance_is_real(self):
        P = np.random.rand(36).reshape((6, 6))
        P = P + P.T + np.eye(6) * 5
        self.assertGreater(np.linalg.det(P), 0)
        self.tested.P = P
        self.tested.Q = np.zeros((6, 6))
        sigma_points = self.tested.sigma_points()
        cov = np.cov(sigma_points.T)
        assert_allclose(cov, P)

    def test_static(self):
        sigma = 0.01
        samples = 300
        target_ang = np.array([0, 0.1, 0])
        for i in range(samples):
            pos = np.random.randn(3) * sigma
            self.tested.add(angles_to_quaternion(target_ang + pos), 1)
        assert_allclose(self.tested.get_angles(), angles_to_quaternion(target_ang), atol=sigma, rtol=0.02)
        assert_allclose(self.tested.get_state().ravel()[4:], np.zeros(3), atol=np.sqrt(sigma), rtol=0.01)

    def test_const(self):
        sigma = 0.001
        samples = 100
        target_ang = np.array([0, 0.0, 0])
        target_rate = np.array([0.2, 0, 0.3])
        target_quat = angles_to_quaternion(target_ang)
        rate_quat = angles_to_quaternion(target_rate)
        for i in range(samples):
            pos = np.random.randn(3) * sigma
            target_ang += target_rate
            target_quat = target_quat.cross(rate_quat)
            target_ang = np.array([a - 2 * np.pi if a > np.pi else a for a in target_ang])
            self.tested.add(target_quat, 1)
            print(self.tested.get_state())

            print(self.tested.P)
            print("*"*50)
        assert_allclose(self.tested.get_state().ravel()[:4], target_quat, atol=sigma, rtol=0.02)
        assert_allclose(self.tested.get_state().ravel()[4:], target_rate, atol=np.sqrt(sigma))


class Movement3dFilterTestsQuat(TestCase):

    def setUp(self):
        self.tested = Movement3dModelQuat()

    def test_static(self):
        sigma = 0.01
        samples = 500
        for i in range(samples):
            pos = np.random.randn(7) * sigma
            pos[3:] = angles_to_quaternion(pos[4:])
            self.tested.update(pos, 1)
            print(self.tested.velocity)
            print(self.tested.last_error)
        correct = np.zeros(13)
        correct[-4] = 1.0
        assert_allclose(self.tested.get_state().ravel(), correct, atol=sigma)

class Movement3dFilterTests(TestCase):

    def setUp(self):
        self.tested = Movement3dModel()

    def test_static(self):
        sigma = 0.01
        samples = 500
        for i in range(samples):
            pos = np.random.randn(6) * sigma
            self.tested.update(pos, 1)
        assert_allclose(self.tested.get_state().ravel(), np.zeros(12), atol=sigma)

    def test_const(self):
        sigma = 0.01
        samples = 100
        dpos = np.array([1, 2, 3, 0.03, 0.02, 0.01])
        pos = np.zeros(6)
        for i in range(samples):
            noise = np.random.randn(6) * sigma
            pos += dpos
            pos[3:] = [x - 2 * np.pi if x > np.pi else x for x in pos[3:]]
            noised = pos + noise
            noised[3:] = [x - 2 * np.pi if x > np.pi else x for x in noised[3:]]
            self.tested.update(noised, 1)
        expected = np.zeros(12)
        expected[0:6] = pos
        expected[6:] = dpos
        assert_allclose(np.concatenate((self.tested.get_state().ravel()[0:3], self.tested.get_state().ravel()[6:9])), pos, rtol=0.1)
