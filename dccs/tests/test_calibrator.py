from unittest import TestCase

import numpy as np
from numpy.testing import assert_allclose
from pyrr import Quaternion

from dccs.math.calibrators import RelativeRotationFitter
import dccs.math.algebra as algebra

class RelativeRotationFitterTest(TestCase):
    def setUp(self):
        self.tested = RelativeRotationFitter()

    @staticmethod
    def rotate_shifted(rotation, shift):
        # shift is transformation from base to rotation frame
        return shift.cross(rotation).cross(shift.conjugate)

    def test_apply_small_for_direct(self):
        shift_angles =  np.random.rand(3)*2*np.pi - np.pi
        qs = algebra.angles_to_quaternion(shift_angles)
        rotation_angles = np.zeros(3)
        rotation = algebra.angles_to_quaternion(rotation_angles)
        error_value = self.tested.apply(qs, (rotation, RelativeRotationFitterTest.rotate_shifted(rotation, qs)))
        self.assertAlmostEqual(error_value, 0, 3)

    def test_apply_small_for_good_model(self):
        shift_angles =  np.random.rand(3)*2*np.pi - np.pi
        qs = algebra.angles_to_quaternion(shift_angles)
        rotation_angles =  np.random.rand(3)*2*np.pi - np.pi
        rotation = algebra.angles_to_quaternion(rotation_angles)
        error_value = self.tested.apply(qs, (rotation, RelativeRotationFitterTest.rotate_shifted(rotation, qs)))
        self.assertAlmostEqual(error_value, 0, 3)

    def test_apply_big_for_bad_model(self):
        shift_angles =  np.random.rand(3)*np.pi
        qs = algebra.angles_to_quaternion(shift_angles)
        rotation_angles =  np.random.rand(3)*2*np.pi - np.pi
        rotation = algebra.angles_to_quaternion(rotation_angles)
        wrong_guess = algebra.angles_to_quaternion(- np.ones(3) * np.pi * 3 / 4.0)
        error_value = self.tested.apply(wrong_guess, (rotation, RelativeRotationFitterTest.rotate_shifted(rotation, qs)))
        self.assertGreater(error_value, 1)

    def test_ransac_plain(self):
        ransac = RelativeRotationFitter()
        shift_angles =  np.random.rand(3)*np.pi
        qs = algebra.angles_to_quaternion(shift_angles)
        data_size = 10
        dataset = []
        for i in range(data_size):
            rotation_angles =  np.random.rand(3)*2*np.pi - np.pi
            rotation = algebra.angles_to_quaternion(rotation_angles)
            dataset.append((rotation, RelativeRotationFitterTest.rotate_shifted(rotation, qs)))
        solution = ransac.fit(dataset, 1000)
        print(solution)
        try:
            assert_allclose(np.array(solution), np.array(qs))
        except AssertionError:
            assert_allclose(np.array(solution), -np.array(qs))

    def test_ransac_noise(self):
        ransac = RelativeRotationFitter(0.08)
        shift_angles = np.random.rand(3)*np.pi
        qs = algebra.angles_to_quaternion(shift_angles)
        print("Target:", qs)
        data_size = 30
        dataset = []
        noise_scale = 0.04
        for i in range(data_size):
            rotation_angles =  np.random.rand(3)*2*np.pi - np.pi
            rotation = algebra.angles_to_quaternion(rotation_angles)
            noise_quat = algebra.angles_to_quaternion((np.random.rand(3)*2*np.pi - np.pi)*noise_scale)
            dataset.append((rotation.cross(noise_quat), RelativeRotationFitterTest.rotate_shifted(rotation, qs)))
        solution = ransac.fit(dataset, 1000, acceptance_ratio=0.99)
        try:
            assert_allclose(np.array(solution), np.array(qs), atol=0.01)
        except AssertionError:
            assert_allclose(np.array(solution), -np.array(qs), atol=0.01)
        print("Solution:", solution)