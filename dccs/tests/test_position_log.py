from unittest import TestCase

import numpy as np
from numpy.testing import assert_equal

from dccs.utils.data import Ringbuffer
from dccs.math.odometry import SlamOdometry


class TestRingbuffer(TestCase):
    def put(self, val):
        self.tested.add(np.ones(3) * val)

    def extend(self):
        for i in range(10):
            self.put(i + 4)

    def setUp(self):
        self.tested = Ringbuffer(10, 3)
        self.put(1)
        self.put(2)
        self.put(3)

    def test_have_correct_range(self):
        data_range = self.tested.get_data_range()
        self.assertEquals(0, data_range[0])
        self.assertEquals(3, data_range[1])

    def test_have_full_correct_range(self):
        self.extend()
        data_range = self.tested.get_data_range()
        self.assertEquals(3, data_range[0])
        self.assertEquals(10, data_range[1])

    def test_returns_lower_bound(self):
        idx = self.tested.get_first_after(1.5)
        print(self.tested.get_entry(idx))
        self.assertEquals(idx, 1)

    def test_returns_upper_bound(self):
        idx = self.tested.get_last_before(1.5)
        print(self.tested.get_entry(idx))
        self.assertEquals(idx, 2)

    def test_returns_bounds_lower_edge(self):
        last_before = self.tested.get_last_before(0.5)
        first_after = self.tested.get_first_after(0.5)
        self.assertEquals(first_after, 2)
        self.assertEquals(last_before, None)

    def test_returns_bounds_upper_edge(self):
        last_before = self.tested.get_last_before(3.5)
        first_after = self.tested.get_first_after(3.5)
        self.assertEquals(first_after, None)
        self.assertEquals(last_before, 0)

    # Full buffer part

    def test_returns_lower_bound_full(self):
        self.extend()
        idx = self.tested.get_first_after(9.5)
        self.assertEquals(idx, 3)

    def test_returns_upper_bound_full(self):
        self.extend()
        idx = self.tested.get_last_before(9.5)
        self.assertEquals(idx, 4)

    def test_returns_bounds_lower_edge_full(self):
        self.extend()
        last_before = self.tested.get_last_before(3.5)
        first_after = self.tested.get_first_after(3.5)
        self.assertEquals(first_after, 9)
        self.assertEquals(last_before, None)

    def test_returns_bounds_upper_edge_full(self):
        self.extend()
        last_before = self.tested.get_last_before(14)
        first_after = self.tested.get_first_after(14)
        self.assertEquals(first_after, None)
        self.assertEquals(last_before, 0)


class TestSlamRingbuffer(TestCase):
    class MockSlamOdometry(SlamOdometry):
        def __init__(self):
            self.tracking_periods = Ringbuffer(5, 2)
            self.position_buffer = Ringbuffer(10, 13)

    def setUp(self):
        slam = TestSlamRingbuffer.MockSlamOdometry()
        slam.position_buffer.add(np.ones(13) * 1)
        slam.position_buffer.add(np.ones(13) * 2)
        slam.position_buffer.add(np.ones(13) * 3)
        slam.tracking_periods.add(np.array([1, np.inf]))
        self.slam = slam

    def test_interpolates_value(self):
        pos15 = self.slam.get_position_by_time(1.5)
        pos18 = self.slam.get_position_by_time(1.8)
        assert_equal(pos15.translation, 1.5)
        assert_equal(pos18.translation, 1.8)

    def test_interpolates_exact_value(self):
        pos10 = self.slam.get_position_by_time(1.0)
        assert_equal(pos10.translation, 1.0)

    def test_extrapolates_value(self):
        pos40 = self.slam.get_position_by_time(4.0)
        assert_equal(pos40.translation, 4.0)

    def test_none_when_lost(self):
        self.slam.tracking_periods.get_entry()[1] = 3
        self.assertIsNone(self.slam.get_position_by_time(4.0))

    def test_none_before_tracking(self):
        self.assertIsNone(self.slam.get_position_by_time(0.0))

    def test_none_when_in_break(self):
        self.slam.tracking_periods.get_entry()[1] = 3
        self.slam.tracking_periods.add([5, np.inf])
        self.slam.position_buffer.add(np.ones(13) * 5)
        self.assertIsNone(self.slam.get_position_by_time(4.0))