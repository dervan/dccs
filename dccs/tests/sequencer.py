from typing import List

import numpy as np
from pyrr import Quaternion

from dccs.math import algebra as agb
from dccs.math.algebra import angles_to_quaternion
from dccs.math.extrinsic import Transformation


class PositionSet:
    def __init__(self, degrees_noise=0, pose_noise=0):
        self.current_base_position: Transformation = Transformation.identity()
        self.positions = []
        self.relations: List[Transformation] = []
        self.radians_noise = degrees_noise * np.pi / 180
        self.pose_noise = pose_noise

    def add_rigid_frame(self, translation, rotation, scale=1.0):
        """Given parameters are relative -> base."""
        t = Transformation(translation, agb.rotation_angles_to_mtx(rotation), scale).inverse()
        self.positions.append(Transformation.identity())
        self.relations.append(t)

    def apply_transform(self, translation, rotation):
        dt0 = Transformation(translation, agb.rotation_angles_to_mtx(rotation))
        self.current_base_position = self.current_base_position.append(dt0)
        # r is transformation FROM base to SLAVE
        for i, r in enumerate(self.relations):
            # So that means slave -(r^-1)-> base -(dt0)-> new base -(r)> new slave
            dt = dt0.around(r)
            self.positions[i] = self.positions[i].append(dt)

    def print_positions(self):
        print("Base: ", self.current_base_position)
        for i, p in enumerate(self.positions):
            print("Position {} : {}".format(i + 1, p))

    def position(self, param):
        if param == 0:
            return self.current_base_position
        else:
            return self.positions[param - 1]

    def scale(self, param):
        return 1 if param == 0 else self.relations[param - 1].scale

    def quaternion(self, param):
        return self.position(param).quaternion

    def translation(self, param):
        return self.position(param).translation

    def noisy_quaternion(self, param) -> Quaternion:
        noise = np.random.randn(3) * self.radians_noise
        quat_noise = angles_to_quaternion(noise)
        return self.quaternion(param) * quat_noise

    def noisy_translation(self, param):
        noise = np.random.randn(3) * self.pose_noise * self.scale(param)
        t = self.translation(param)
        return t + noise

    def noisy_transformation(self, param):
        return Transformation(self.noisy_translation(param), self.noisy_quaternion(param).matrix33)

    def transformation(self, param):
        return Transformation(self.translation(param), self.quaternion(param).matrix33)


class RandomDisturbedConst:
    def __init__(self, translation_distribution, rotation_distribution):
        self.t_dist = translation_distribution
        self.r_dist = rotation_distribution
        self.position = np.zeros(7)

    def __iter__(self):
        dt = np.random.randn(3) * self.t_dist[1] + self.t_dist[0]
        dq = np.random.randn(4) * self.r_dist[1] + self.r_dist[0]
        self.position[0:3] += dt
        self.position[3:] += dq
        return self.position
