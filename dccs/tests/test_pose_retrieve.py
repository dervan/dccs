import numpy as np

import dccs.math.algebra as algebra
import dccs.tests.sequencer
from dccs.math.extrinsic import estimate_relation

degrees_noise = 5
pose_noise = 0.01
n = 100
iters = 100
for pose_noise in [0, 0.01, 0.05]:
    for degrees_noise in [0, 1, 5]:
        for n in [5, 15]:
            errors = np.zeros(3)
            for _ in range(iters):
                ps = dccs.tests.sequencer.PositionSet(degrees_noise=degrees_noise, pose_noise=pose_noise)

                start_angles = np.array([-1.8, np.pi / 5, 0.2])
                start_pos = np.array([0.5, 10, 2])
                start_quat = algebra.angles_to_quaternion(start_angles)

                # scale base => relative
                start_scale = 2.66

                ps.add_rigid_frame(start_pos, start_angles, start_scale)
                base = []
                relative = []
                for i in range(n):
                    random_translation = np.random.rand(3) * 2 - 1
                    random_rotation = np.random.rand(3) * np.pi / 8
                    ps.apply_transform(random_translation, random_rotation)
                    base.append(ps.noisy_transformation(0))
                    relative.append(ps.noisy_transformation(1))

                relation = estimate_relation(base, relative)
                tmp = np.zeros(3)
                # pose error
                tmp[0] = algebra.l2_norm(algebra.normalized_l2(relation.translation) - algebra.normalized_l2(start_pos))

                # orientation error
                if relation.quaternion.dot(start_quat) < 1.0:
                    tmp[1] = 2 * np.arccos(relation.quaternion.dot(start_quat))

                # scale error
                tmp[2] = abs(relation.scale / start_scale - 1)
                errors += tmp

            errors /= iters
            print("{} & {} & {} & {:.4} & {:.4} & {:.3}\\% \\\\".format(n, pose_noise, degrees_noise,
                                                                        errors[0], errors[1], errors[2]*100))
