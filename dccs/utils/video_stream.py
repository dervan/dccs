import os
import threading
from time import sleep, time

import cv2
import numpy as np
from metrology import Metrology

from ..math.camera_model import CameraData
from ..config import camera_data_dir


class VideoFeeder(threading.Thread):
    """
    Main class to link hardware and software layers. It reads frames from device in regular way and ackonowledges
    threads which wants to know if new frame is available.
    """

    def __init__(self, cam_data, fps=30):
        super(VideoFeeder, self).__init__()
        self.device = cv2.VideoCapture("/dev/video" + str(cam_data.v4l2_num))
        self.xres = cam_data.xres
        self.yres = cam_data.yres
        self.device.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'))
        self.device.set(cv2.CAP_PROP_FRAME_WIDTH, cam_data.xres)
        self.device.set(cv2.CAP_PROP_FRAME_HEIGHT, cam_data.yres)
        self.device.set(cv2.CAP_PROP_FPS, cam_data.fps)
        sleep(1)
        self.ret = None
        self.sleep_time = float(1.0 / fps)
        self.frame = np.zeros((self.yres, self.xres))
        self.waiting = threading.Condition()
        self.cv_lock = threading.Lock()
        self.frame2 = self.frame
        self.camera = cam_data.get_base_camera()
        self.maps = self.camera.get_undistort_maps()
        print("Video feeder for cam{} launched!".format(cam_data.v4l2_num))
        self._stop_request = threading.Event()

    def read(self):
        self.ret, self.frame = self.device.read()
        # self.frame = np.float32(self.frame)
        return self.ret, self.frame

    def release(self):
        self.device.release()

    def stop(self):
        self._stop_request.set()

    def run(self):
        frames = Metrology.meter('raw-frames')
        while True:
            next_frame = time() + self.sleep_time
            self.read()
            frames.mark()
            self.waiting.acquire()
            self.waiting.notify_all()
            self.waiting.release()
            act_sleep = next_frame - time()
            if act_sleep > 0:
                sleep(act_sleep)
            if self._stop_request.is_set():
                self.release()
                return

    def get_resolution(self):
        return self.frame.shape

    def get_frame(self):
        return self.frame

    def get_sleep_time(self):
        return self.sleep_time

    def get_wait_conditional(self):
        return self.waiting

    def undistort(self):
        self.frame2 = cv2.remap(self.frame, *self.maps, cv2.INTER_CUBIC)


class VideoConsumer(threading.Thread):
    def __init__(self, video_feeder):
        super(VideoConsumer, self).__init__()
        self.video_feeder = video_feeder
        self.waiting = video_feeder.get_wait_conditional()

    def run(self):
        while True:
            frame = self.video_feeder.get_frame()
            self.waiting.acquire()
            self.waiting.wait()
            self.waiting.release()
            if not self.process(frame):
                break

    def process(self, frame: np.ndarray):
        raise NotImplementedError


class RawPresenter(VideoConsumer):
    def __init__(self, video_feeder, name="Presenter"):
        super(RawPresenter, self).__init__(video_feeder)
        self.name = name

    def process(self, frame):
        with self.video_feeder.cv_lock:
            cv2.imshow(self.name, frame)
            key = cv2.waitKey(10)
            if key == 27:
                print(self.name + " exits!")
                cv2.destroyAllWindows()
                return False
        return True


class VideoStream:
    def __init__(self, cam_data: CameraData, mode="raw"):
        self.camera_data = cam_data
        self.device = cv2.VideoCapture("/dev/video" + str(cam_data.v4l2_num))
        self.xres = cam_data.xres
        self.yres = cam_data.yres
        self.fps = cam_data.fps
        self.device.set(cv2.CAP_PROP_FRAME_WIDTH, cam_data.xres)
        self.device.set(cv2.CAP_PROP_FRAME_HEIGHT, cam_data.yres)
        self.device.set(cv2.CAP_PROP_FPS, cam_data.fps)
        self.device.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc(*cam_data.format))
        sleep(1)
        self.ret = None
        self.frame = np.zeros(self.camera.yx_shape)
        self.mode = mode
        print("Start {}x{} stream, {} fps".format(self.xres, self.yres, self.fps))

    @property
    def name(self):
        return self.camera_data.name

    @property
    def camera(self):
        return self.camera_data.get_base_camera()

    def read(self, wait_timer=1):
        while wait_timer > 0:
            self.ret, frame = self.device.read()
            wait_timer -= 1
        if self.ret:
            self.frame = frame
        else:
            print("Error during frame reading")
        if self.mode == 'rec':
            self.frame = self.camera.undistort(self.frame)
        return True, self.frame

    def release(self):
        self.device.release()

    def save(self, filename=None, dir=None):
        if dir is None:
            dir = "photos"

        photosdir =  camera_data_dir(self.name, dir) + "/"
        if filename is None:
            filename = "image" + str(len(os.listdir(photosdir))) + ".png"
        full_name = photosdir + filename
        cv2.imwrite(full_name, self.frame)
        print("Image", filename, "saved.")
        return full_name

    def save_stereo(self):
        return self.save(dir="stereo_photos")


class VideoSender:
    def __init__(self, resolution, target_ip, target_port):
        assert isinstance(resolution, tuple)
        assert isinstance(target_ip, str)
        assert isinstance(target_port, str)
        self.resolution = resolution[1], resolution[0]
        gst_string = "appsrc ! videoconvert !"\
                     " x264enc noise-reduction=10000 speed-preset=ultrafast tune=zerolatency !"\
                     " rtph264pay config-interval=1 pt=96 ! udpsink host={} port={} sync=false"
        self.writer = cv2.VideoWriter(gst_string.format(target_ip, target_port), 0, 30.0, self.resolution, True)

    def send(self, frame):
        self.writer.write(frame)


class VideoSenderConsumer(VideoConsumer):
    def __init__(self, video_feeder, target_ip, target_port, name="VideoSenderPresenter"):
        super(VideoSenderConsumer, self).__init__(video_feeder)
        self.name = name
        self.sender = VideoSender(video_feeder.get_resolution(), target_ip, target_port)
        print("Video sender to {}:{} launched!".format(target_ip, target_port))

    def process(self, frame):
        self.sender.send(frame)
        return True
