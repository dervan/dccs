"""
This module should contain various image-related functions. That means
that functions here should work purely in image domain.
"""

import cv2
import scipy.ndimage as nimg
import numpy as np
import scipy.stats as st
from typing import List


##############################################################################
# Image loading and showing functions.
##############################################################################

def plt_show(img, title="img"):
    import matplotlib.pyplot as plt
    fig=plt.figure(figsize=(18, 18), dpi= 80)
    plt.title(title)
    plt.imshow(img)

def cv_show(img: np.ndarray, max_width: int = 1280, time: int = 0) -> None:
    """Shows image, potentially scaled to fit max_width. Function will wait at
     most time ms (unlimited when equals 0)"""
    if img.shape[1] > max_width:
        cv2.imshow("Image", fit_photos([img], max_width))
    else:
        cv2.imshow("Image", img)
    wait_for_esc(time)


def wait_for_esc(time_limit: int = 0) -> None:
    """Waits at most time_limit for key. If zero is given waits for unlimited time"""
    if time_limit == 0:
        while cv2.waitKey(0) != 27:
            pass
    else:
        cv2.waitKey(time_limit)


def load_gray(imgName: str) -> np.ndarray:
    """Loads image and returns grayscale version of image"""
    image = cv2.imread(imgName)
    return cv2.cvtColor(image, cv2.COLOR_RGB2GRAY).astype('float')


def load_rgb(imgName: str) -> np.ndarray:
    """Loads image and returns RGB version of image"""
    image = cv2.imread(imgName)
    return cv2.cvtColor(image, cv2.COLOR_BGR2RGB)


def load_bgr(imgName: str) -> np.ndarray:
    """Loads image and returns BGR version of image"""
    image = cv2.imread(imgName)
    return image


def normalize(img: np.ndarray) -> np.ndarray:
    """Scales all values in image to range 0-255"""
    nmin = np.min(img)
    nmax = np.max(img)
    return 255 * (img - nmin) / (nmax - nmin)


def draw_points(image, points, r=2, w=1, color=(255, 10, 10)):
    """Draws points on copy of given image"""
    withPoints = image.copy()
    for u in points:
        cv2.circle(withPoints, (int(u[0]), int(u[1])), r, color, 2)
    return withPoints


def show_points(image, points, r=2, w=1, color=(255, 10, 10)):
    """Draws and shows points on given image"""
    with_points = draw_points(image, points, r, w, color)
    cv_show(with_points)


def show_points_size(image, points, colors=None):
    """Show points on image, where every point is given as tuple (point, radius)"""
    with_points = image.copy()
    for i, (point, r) in enumerate(points):
        if colors is None:
            c = (255, 80, 50)
        else:
            c0 = (255, 0, 0)
            c = tuple(map(int, colors[i % len(colors)]))
        if not isinstance(point, tuple):
            tmp = point
            point = r
            r = tmp
        cv2.circle(with_points, (int(point[1]), int(point[0])), int(r), color=c, thickness=3)
        cv2.circle(with_points,
                   (int(point[1]), int(point[0])), 1, (150, 80, 50), 2)
    cv_show(with_points)


def draw_epilines(img1, img2, lines, pts1, pts2):
    """ img1 - image on which we draw the epilines for the points in img2
        lines - corresponding epilines """
    r = img1.shape[0]
    c = img1.shape[1]
    # img1 = cv2.cvtColor(img1,cv2.COLOR_GRAY2BGR)
    # img2 = cv2.cvtColor(img2,cv2.COLOR_GRAY2BGR)
    for r, pt1, pt2 in zip(lines, pts1, pts2):
        color = tuple(np.random.randint(0, 255, 3).tolist())
        x0, y0 = map(int, [0, -r[2] / r[1]])
        x1, y1 = map(int, [c, -(r[2] + r[0] * c) / r[1]])
        img1 = cv2.line(img1, (x0, y0), (x1, y1), color, 1)
        img1 = cv2.circle(img1, tuple(pt1), 5, color, -1)
        img2 = cv2.circle(img2, tuple(pt2), 5, color, -1)
    return img1, img2


def fit_photos(photos: List[np.ndarray], target_width=0) -> np.ndarray:
    """Stack images together and scale them to fit given target_width"""
    size = 0
    dcount = len(photos[0].shape)
    for p in photos:
        size += p.shape[1]
    scale = target_width / size
    if scale == 0:
        scale = 1
    max_height = max(map(lambda photo: int(photo.shape[0] * scale), photos))
    imgs = list(
        map(lambda photo: cv2.resize(photo, (int(scale * photo.shape[1]), int(scale * photo.shape[0]))), photos))
    if dcount == 2:
        imgs = map(lambda photo: np.append(photo, np.zeros((max_height - photo.shape[0], photo.shape[1])), axis=0),
                   imgs)
    else:
        imgs = map(
            lambda photo: np.append(photo, np.zeros((max_height - photo.shape[0], photo.shape[1], 3), dtype='u1'),
                                    axis=0), imgs)
    return np.hstack(imgs)


def show_correspondences(left, right, lpoints, rpoints, colors=None):
    """Shows image with given correspondences"""
    width = int(np.ceil(max(left.shape) * 0.001))
    if (left.shape[0] <= right.shape[0]):
        newLeft = np.zeros((right.shape[0], left.shape[1], 3))
        newLeft[0:left.shape[0]] = left[:]
        left = newLeft
    elif (left.shape[0] > right.shape[0]):
        newRight = np.zeros((left.shape[0], right.shape[1], 3))
        newRight[0:right.shape[0]] = right[:]
        right = newRight

    img = np.hstack([left, right])
    shift = left.shape[1]
    for i, ((lpoint, lr), (rpoint, rr)) in enumerate(zip(lpoints, rpoints)):
        if colors is None:
            c = (255, 80, 50)
        else:
            c0 = (255, 0, 0)
            c = tuple(map(int, colors[i % len(colors)]))
        if not isinstance(lpoint, tuple):
            tmp = lpoint
            lpoint = lr
            lr = tmp
        if not isinstance(rpoint, tuple):
            tmp = rpoint
            rpoint = rr
            rr = tmp
        cv2.circle(img, (int(lpoint[1]), int(lpoint[0])), int(lr), color=c, thickness=3)
        cv2.circle(img, (int(rpoint[1] + shift), int(rpoint[0])), int(rr), color=c, thickness=3)
        cv2.circle(img, (int(lpoint[1]), int(lpoint[0])), 1, (150, 80, 50), 2)
        cv2.circle(img, (int(rpoint[1] + shift), int(rpoint[0])), 1, (150, 80, 50), 2)
        cv2.line(img, (int(lpoint[1]), int(lpoint[0])), (int(rpoint[1] + shift), int(rpoint[0])), color=c,
                 thickness=width)
    cv_show(img.astype('uint8'), 2)


##############################################################################
# Image processing helper functions
##############################################################################

def blur(img: np.ndarray, sig: float) -> np.ndarray:
    """Return blurred version of image"""
    return nimg.gaussian_filter(img, sig, 0)


def gaussian_kernel(kernlen: int = 16, nsig: int = 1) -> np.ndarray:
    """Returns generated gaussian with given radius"""
    interval = (2 * nsig + 1.) / (kernlen)
    x = np.linspace(-nsig - interval / 2., nsig + interval / 2., kernlen + 1)
    kern1d = np.diff(st.norm.cdf(x))
    kernel_raw = np.sqrt(np.outer(kern1d, kern1d))
    kernel = kernel_raw / kernel_raw.sum()
    return kernel


def gray(img: np.ndarray) -> np.ndarray:
    """Converts image to grayscale if array is 3D/4D and does nothing if it's 2D"""
    if img.ndim == 2:
        return img
    else:
        return cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)


def color(gray_img: np.ndarray) -> np.ndarray:
    """Converts grayscale image to RGB"""
    return cv2.cvtColor(gray_img, cv2.COLOR_GRAY2RGB)


##############################################################################
# Matching related functions
##############################################################################
_debug = False


def ssd(imgA, imgB, a, b, patchsize):
    ym = np.floor(min(patchsize, a[0], b[0])).astype('int32')
    xm = np.floor(min(patchsize, a[1], b[1]))
    yp = np.floor(min(patchsize, imgA.shape[0] - a[0] - 1, imgB.shape[0] - b[0] - 1))
    xp = np.floor(min(patchsize, imgA.shape[1] - a[1] - 1, imgB.shape[1] - b[1] - 1))
    return np.sum(np.abs(imgA[a[0] - ym:a[0] + yp + 1, a[1] - xm:a[1] + xp + 1] -
                         imgB[b[0] - ym:b[0] + yp + 1, b[1] - xm:b[1] + xp + 1])) / ((ym + yp + 1) * (xm + xp + 1))


def pixelDist(X, Y):
    return np.exp(-np.linalg.norm(X - Y, ord=1) / 100) * np.linalg.norm(X - Y)


def matchCost(imgA, imgB, a, b, patchsize):
    ym = np.floor(min(patchsize, a[0], b[0])).astype('int32')
    xm = np.floor(min(patchsize, a[1], b[1])).astype('int32')
    yp = np.floor(min(patchsize, imgA.shape[0] - a[0] - 1, imgB.shape[0] - b[0] - 1)).astype('int32')
    xp = np.floor(min(patchsize, imgA.shape[1] - a[1] - 1, imgB.shape[1] - b[1] - 1)).astype('int32')
    return np.sum(np.abs(imgA[a[0] - ym:a[0] + yp + 1, a[1] - xm:a[1] + xp + 1] -
                         imgB[b[0] - ym:b[0] + yp + 1, b[1] - xm:b[1] + xp + 1]) * np.exp(
        -(imgB[b[0] - ym:b[0] + yp + 1, b[1] - xm:b[1] + xp + 1] - imgA[a[0], a[1]]) / 100)) / (
               (ym + yp + 1) * (xm + xp + 1))


def compute_stereo_energy(disp, left, right, patchsize, penalty, maxd):
    y, x = disp.shape
    energy = 0.0
    ngh = [(-1, 0), (1, 0), (0, -1), (0, 1)]
    for i in range(1, y - 1):
        for j in range(1, x - maxd):
            if (j + disp[i, j] < x):
                energy += (float(left[i, j]) - float(right[i, j + disp[i, j]])) ** 2
            energy += np.sum([penalty * (disp[i + di, j + dj] != disp[i, j]) for di, dj in ngh])
    return energy


def correspondences(left_photo: np.ndarray, right_photo: np.ndarray) -> None:
    """Try to find corresponding features on both photos"""
    if left_photo is None or right_photo is None or len(left_photo.shape) != 2 or len(right_photo.shape) != 2:
        print("Invalid photos given to calculate correspondencies")
        return None

    # Find features
    sift1 = cv2.xfeatures2d.SIFT_create(edgeThreshold=7)
    sift2 = cv2.xfeatures2d.SIFT_create(edgeThreshold=7)
    right_points, right_desc = sift1.detectAndCompute(right_photo, None)
    left_points, left_desc = sift2.detectAndCompute(left_photo, None)
    if True:
        print("Found {} features on first image".format(len(left_desc)))
        print("Found {} features on second image".format(len(right_desc)))

    if right_desc is None or left_desc is None:
        print("Cannot find features!")
        return None

    # FLANN parameters
    FLANN_INDEX_KDTREE = 0
    index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
    search_params = dict(checks=50)

    # flann = cv2.FlannBasedMatcher(index_params, search_params)
    bf = cv2.BFMatcher()

    matches = bf.knnMatch(right_desc, left_desc, k=2)
    # matches = sorted(matches, key = lambda x: x.distance)
    right_selected = []
    left_selected = []
    # matched = cv2.drawMatches(right_photo, right_points, left_photo, left_points, matches[:500], None, flags=2)
    # cv_show(matched, 1920)
    matched_descriptors = []
    # ratio test as per Lowe's paper
    for m, n in matches:
        if m.distance < 0.8 * n.distance:
            if left_points[m.trainIdx].pt == right_points[m.queryIdx].pt:
                print(left_points[m.trainIdx].pt)
                print("Something is suspicious ;/")
            # print(left_points[m.trainIdx].pt, " <=> ", right_points[m.queryIdx].pt)
            # print(list(zip(left_desc[m.trainIdx], right_desc[m.queryIdx])))
            left_selected.append(left_points[m.trainIdx].pt)
            right_selected.append(right_points[m.queryIdx].pt)
            matched_descriptors.append(left_desc[m.trainIdx])
    right_selected = np.array(right_selected, dtype=np.float32)
    left_selected = np.array(left_selected, dtype=np.float32)
    matched_descriptors = np.int32(matched_descriptors)

    return left_selected, right_selected, matched_descriptors


def fundamental_from_photos(left_photo: np.ndarray, right_photo: np.ndarray, debug_filename: str = None) -> np.ndarray:
    """Estimates fundamental matrix from two given grayscale images"""
    if left_photo is None or right_photo is None or len(left_photo.shape) != 2 or len(right_photo.shape) != 2:
        print("Invalid photos given to fundamental_from_photos")
        return None

    # Firstly find corresponding points
    left_selected, right_selected, descriptors = correspondences(left_photo, right_photo)
    # for i in range(min([len(left_selected), 30])):
    #    print(left_selected[i], " <=> ", right_selected[i])

    if len(right_selected) < 9 or len(left_selected) < 9:
        print("Not enough good matches!")
        return None

    # Estimate F, with RANSAC and all stuff like that.
    F, mask = cv2.findFundamentalMat(left_selected, right_selected, cv2.FM_RANSAC, param1=1, param2=0.999999)

    if F is None or F.shape != (3, 3):
        print("Invalid F!")
        return None

    if debug_filename is None:
        return F

    # We select only inlier points
    a = mask.ravel() == 1
    b = mask.ravel() == 1
    right_selected = right_selected[a]
    left_selected = left_selected[b]
    descriptors_selected = descriptors[b]

    # Find epilines corresponding to points in right image (second image) and
    # drawing its lines on left image
    lines1 = cv2.computeCorrespondEpilines(left_selected.reshape(-1, 1, 2), 2, F)
    lines1 = lines1.reshape(-1, 3)
    right_copy = np.array(right_photo)
    left_copy = np.array(left_photo)
    img5, img6 = draw_epilines(left_copy, right_copy, lines1, left_selected, right_selected)

    # Find epilines corresponding to points in left image (first image) and
    # drawing its lines on right image
    lines2 = cv2.computeCorrespondEpilines(right_selected.reshape(-1, 1, 2), 1, F)
    lines2 = lines2.reshape(-1, 3)
    img3, img4 = draw_epilines(right_copy, left_copy, lines2, right_selected, left_selected)
    r1 = fit_photos([img5, img3], 1920)
    r2 = fit_photos([img6, img4], 1920)
    photo = np.vstack([r1, r2])
    cv2.imwrite(debug_filename, photo)
    return F, list(zip(left_selected, right_selected)), descriptors_selected
