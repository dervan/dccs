import asyncio
import functools
import signal

import aiomas
from .data import dccs_serializers


async def shutdown(sig, loop):
    # shutdown from https://gist.github.com/nvgoldin/30cea3c04ee0796ebd0489aa62bcf00a
    print('caught {0}'.format(sig.name))
    tasks = [task for task in asyncio.Task.all_tasks() if task is not
             asyncio.tasks.Task.current_task()]
    list(map(lambda task: task.cancel(), tasks))
    results = await asyncio.gather(*tasks, return_exceptions=True)
    print('finished awaiting cancelled tasks, results: {0}'.format(results))
    loop.stop()


def prepare_shutdown_hooks():
    loop = asyncio.get_event_loop()
    loop.add_signal_handler(signal.SIGINT, functools.partial(asyncio.ensure_future, shutdown(signal.SIGTERM, loop)))


def create_container(address, port):
    return aiomas.Container.create((address, port),
                                   codec=aiomas.MsgPack,
                                   extra_serializers=dccs_serializers)

