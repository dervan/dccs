import threading
from typing import Union, Tuple, Optional

import numpy as np

from ..math.extrinsic import Transformation


def np_serializer():
    """Return a tuple *(type, serialize(), deserialize())* for NumPy arrays
    for usage with an :class:`aiomas.codecs.MsgPack` codec.

    """
    return np.ndarray, _serialize_ndarray, _deserialize_ndarray


def _serialize_ndarray(obj):
    return {
        'type': obj.dtype.str,
        'shape': obj.shape,
        'data': obj.tostring(),
    }


def _deserialize_ndarray(obj):
    array = np.fromstring(obj['data'], dtype=np.dtype(obj['type']))
    return array.reshape(obj['shape'])


def _deserialize_transformation(obj) -> Transformation:
    return Transformation.from_inlined_quaternion(_deserialize_ndarray(obj))


def _serialize_transformation(obj: Transformation):
    return _serialize_ndarray(obj.inline_quaternion)


def transformation_serializer():
    return Transformation, _serialize_transformation, _deserialize_transformation


dccs_serializers = [np_serializer, transformation_serializer]


class Ringbuffer:
    def __init__(self, size: int, entry_dimension: int):
        self._index = np.zeros(1, dtype='i4')
        self.data_range = np.zeros(2, dtype='i4')
        self.data = np.zeros((size, entry_dimension))
        self.lock = threading.Lock()
        self.full = False

    @property
    def index(self):
        """index always points to first free slot"""
        return self._index[0]

    @property
    def row(self) -> np.ndarray:
        return self.data[self.index]

    @property
    def size(self) -> int:
        return self.data.shape[0]

    @property
    def entry_size(self) -> int:
        return self.data.shape[1]

    def get_data_range(self) -> Tuple[int, int]:
        if self.full:
            return self.index, self.size
        else:
            return 0, self.index

    def inc(self):
        self._index[0] = (self.index + 1) % self.size
        if not self.full:
            self.full = self.index == 0

    def add(self, entry: Union[np.ndarray, list]):
        self.data[self.index] = entry
        self.inc()

    def get_entry(self, shift: int = 0) -> np.ndarray:
        """ For shift == 0 it should return last entry"""
        index = (self.index - shift - 1) % self.size
        return self.data[index]

    def get_entries(self, index_start: int, index_end: int) -> np.ndarray:
        if index_start <= index_end:
            return self.data[index_start:index_end]
        else:
            return np.vstack([self.data[index_start:-1], self.data[:index_end]])

    def get_bounds(self, timestamp: float, column: int = 0) -> Tuple[Optional[int], Optional[int]]:
        lower = 0
        if self.full:
            upper = self.size - 1
        else:
            upper = self.index - 1
        # Searching by back offset in buffer
        while upper - lower > 1:
            idx = lower + (upper - lower) // 2
            if self.get_entry(idx)[column] < timestamp:
                upper = idx
            else:
                lower = idx

        if self.get_entry(upper)[column] > timestamp:
            return upper, None

        if self.get_entry(lower)[column] < timestamp:
            return None, lower

        return lower, upper

    def get_first_after(self, timestamp, column: int = 0) -> int:
        return self.get_bounds(timestamp, column)[0]

    def get_last_before(self, timestamp, column: int = 0) -> int:
        return self.get_bounds(timestamp, column)[1]
