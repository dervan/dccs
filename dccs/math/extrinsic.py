from typing import List

import numpy as np
import scipy.optimize
from pyrr import Quaternion, Matrix33

from . import algebra


class Transformation:
    """Class which describes position of rigid object in the space. It is equivalent to rotation with self.rotation_mtx
    and translation by self.translation IN ROTATED COORDINATES frame.

    To get NOT ROTATED translation get pure_translation
    """
    INLINED_SIZE = 6
    INLINED_QUATERNION_SIZE = 7
    INLINED_FULL_SIZE = 8

    def __init__(self, latter_translation: np.ndarray, rotation_mtx: np.ndarray, scale: float = 1.0):
        assert latter_translation.shape == (3,)
        assert rotation_mtx.shape == (3, 3)
        assert isinstance(scale, float) or isinstance(scale, np.float32), "Scale have to be float, not" + str(
            type(scale))
        if isinstance(rotation_mtx, Matrix33):
            self.rotation_mtx = np.array(rotation_mtx)
        else:
            self.rotation_mtx = rotation_mtx
        self.translation = latter_translation
        self.scale = scale

    def __str__(self):
        return "Transformation{ shift:" + str(self.pure_translation) + ", rot:" + str(self.angles) \
               + ", scale: " + str(self.scale) + "}"

    def __repr__(self):
        return str(self)

    @property
    def norm(self):
        return Transformation(algebra.normalized_l2(self.translation), self.rotation_mtx,
                              self.scale * algebra.l2_norm(self.translation))

    @property
    def pure_translation(self):
        return self.rotation_mtx.dot(self.translation)

    @property
    def full(self):
        # Note, that translation here is rotated!
        full = np.eye(4)
        full[0:3, 0:3] = self.scale * self.rotation_mtx
        full[0:3, -1] = self.translation
        return full

    @property
    def angles(self):
        return algebra.rotation_mtx_to_angles(self.rotation_mtx)

    @property
    def quaternion(self) -> Quaternion:
        return Quaternion.from_matrix(self.rotation_mtx)

    @property
    def pure_inline(self):
        pos = np.zeros(Transformation.INLINED_SIZE)
        pos[0:3] = self.pure_translation
        pos[3:6] = self.angles
        return pos

    @property
    def inline(self):
        pos = np.zeros(Transformation.INLINED_SIZE)
        pos[0:3] = self.translation
        pos[3:6] = self.angles
        return pos

    @property
    def inline_quaternion(self):
        pos = np.zeros(Transformation.INLINED_QUATERNION_SIZE)
        pos[0:3] = self.translation
        pos[3:7] = self.quaternion
        return pos

    @property
    def inline_full(self):
        pos = np.zeros(Transformation.INLINED_FULL_SIZE)
        pos[0:3] = self.translation
        pos[3:7] = self.quaternion
        pos[7] = self.scale
        return pos

    def rescaled(self, scale):
        return Transformation(self.translation * scale, self.rotation_mtx)

    def inverse(self):
        return Transformation.from_full(np.linalg.inv(self.full))

    def around(self, r: "Transformation") -> "Transformation":
        """Return same transformation, but for base point in rigidly connected coordinate frame r"""
        return Transformation.from_full(r.full.dot(self.full).dot(np.linalg.inv(r.full)))

    def substract(self, correction_pos: "Transformation") -> "Transformation":
        pos_corrected = Transformation.from_full(self.full.dot(np.linalg.inv(correction_pos.full)))
        return pos_corrected

    def append(self, transformation: "Transformation") -> "Transformation":
        return Transformation.from_full(transformation.full.dot(self.full))

    @classmethod
    def from_inlined(cls, inlined: np.ndarray) -> 'Transformation':
        assert inlined.shape == (6,)
        pos = inlined[0:3]
        rot = algebra.rotation_angles_to_mtx(inlined[3:])
        return cls(pos, rot)

    @classmethod
    def from_inlined_quaternion(cls, inlined_quaternion: np.ndarray, scale: float = 1.0) -> 'Transformation':
        assert inlined_quaternion.shape == (7,)
        pos = inlined_quaternion[0:3]
        quaternion = Quaternion(inlined_quaternion[3:])
        return cls(pos, quaternion.matrix33, scale)

    @classmethod
    def from_inlined_full(cls, inlined_full: np.ndarray) -> 'Transformation':
        assert inlined_full.shape == (8,)
        return cls.from_inlined_quaternion(inlined_full[0:7], inlined_full[7])

    @classmethod
    def from_full(cls, full_position_matrix: np.ndarray) -> 'Transformation':
        # Full matrix representation is a bit tricky - translation here is rotated!
        assert full_position_matrix.shape == (4, 4)
        pos = full_position_matrix[0:3, -1]
        rot = full_position_matrix[0:3, 0:3]
        scale = np.cbrt(np.linalg.det(rot))
        return cls(pos, rot / scale, scale)

    @classmethod
    def interpolate(cls, position_a: "Transformation", position_b: "Transformation",
                    position_a_weight: float) -> "Transformation":
        quaternion = position_a.quaternion.slerp(position_b.quaternion, position_a_weight)
        translation = position_a.translation * position_a_weight + position_b.translation * (1 - position_a_weight)
        return cls(translation, quaternion.matrix33)

    @classmethod
    def identity(cls) -> "Transformation":
        return cls(np.zeros(3), np.eye(3))

    @staticmethod
    def average_transformation(positions: List["Transformation"]) -> "Transformation":
        if not any(positions):
            return None
        pure_transformations = np.array([pos.pure_translation for pos in positions])
        quats = [pos.quaternion for pos in positions]
        scales = [pos.scale for pos in positions]
        avg_transformation = algebra.geometric_median(pure_transformations, eps=1e-3)
        avg_rotation = algebra.average_quaternion(quats)
        avg_scale = np.average(scales)
        rotated_translation = np.linalg.inv(avg_rotation.matrix33).dot(avg_transformation)
        return Transformation(rotated_translation, avg_rotation.matrix33, avg_scale)

    def transform_points(self, points):
        assert points.shape[1] == 3
        hmg = np.ones((points.shape[0], 4))
        hmg[:, :-1] = points
        transformed = self.full.dot(hmg.T).T
        return transformed[:, :-1]


def nonlinear_refinement(As, Bs, relative_quat, relative_pose, base_translations):
    def error_functional(x):
        q = np.array(algebra.angles_to_quaternion(x[0:3]))
        c = x[3:]
        return sum([algebra.l2_norm(As[4 * i:4 * (i + 1)].dot(q)) ** 2 +
                    algebra.l2_norm(Bs[3 * i:3 * (i + 1)].dot(c) - base_translations[i]) ** 2 for i in range(n)])

    n = len(As) // 4
    x0 = np.zeros(7)
    x0[0:3] = algebra.quaternion_to_angles(relative_quat)
    x0[3:] = relative_pose
    x0_result = scipy.optimize.minimize(error_functional, x0, method='Nelder-Mead', tol=1e-6)
    np.set_printoptions(precision=7)
    # method="trust-constr",
    # constraints=({'type': 'eq', 'fun': lambda x: algebra.l2_norm(x[0:4]) == 1}))

    if x0_result.success:
        print("Refined! {} => {}".format(x0, x0_result.x))
        x0ref = x0_result.x
        print(x0ref - x0)
    else:
        print("Not refined!", x0_result.x)
        x0ref = x0
    return algebra.angles_to_quaternion(x0ref[0:3]), x0ref[3:]


def estimate_relation(corrected_local_positions: List[Transformation],
                      corrected_remote_positions: List[Transformation]) -> Transformation:
    remote_quats = [p.quaternion for p in corrected_remote_positions]
    local_quats = [p.quaternion for p in corrected_local_positions]

    remote_translations = [p.translation for p in corrected_remote_positions]
    local_translations = [p.translation for p in corrected_local_positions]
    relative_quat, As = algebra.estimate_relative_rotation_with_matrix(local_quats, remote_quats)
    relative_pose, Bs = algebra.estimate_relative_position_with_matrix(local_translations, remote_translations,
                                                                       local_quats, relative_quat)

    # relative_quat, relative_pose = nonlinear_refinement(As, Bs, relative_quat, relative_pose, local_translations)

    full_inlined_pose = np.zeros(7)
    full_inlined_pose[0:3] = relative_pose[0:3]
    full_inlined_pose[3:] = relative_quat
    np.set_printoptions(precision=4)
    result = Transformation.from_inlined_quaternion(full_inlined_pose, scale=relative_pose[3])
    if False:
        print("$" * 100)
        print("$" * 100)
        print("$" * 100)
        for l, r in zip(corrected_local_positions, corrected_remote_positions):
            print("Loc: ", l)
            print("Rem: ", r)
            print("Loc * result", result.append(l))
            print("Rem * result", r.append(result))
            print("#" * 100)
    return result
