from math import atan2, asin
from typing import Tuple, List, Union

import numpy as np
import numpy.linalg as lin
from pyrr import Quaternion, Matrix33
from scipy.spatial.distance import cdist, euclidean

"""
This file contains few functions which main subject are vectors and matrices.
"""


def l2_norm(v: np.ndarray) -> float:
    """Calculates euclidean length of vector."""
    return np.sqrt(sum(v ** 2))


def normalized_l2(v: np.ndarray) -> np.ndarray:
    """Returns unit-length vector"""
    return v / l2_norm(v)


def l2_dist(l: np.ndarray, p: np.ndarray) -> float:
    """Calculates euclidean distance between two points."""
    return l2_norm(l - p)


def to_hmg(v: np.ndarray) -> np.ndarray:
    """Converts normal vector to normalized homogeneous vector by appending one on end of vector."""
    return np.append(v, 1)


def from_hmg(v: np.ndarray) -> np.ndarray:
    """Converts normal vector to normalized homogeneous vector by appending one on end of vector."""
    return hmg_canonical(v)[0:-1]


def hmg_canonical(v: np.ndarray) -> np.ndarray:
    """ Converts homogeneous vector to its canonical form which implies one as last coordinate."""
    return v / v[-1]


def rotate_2d(vec: np.ndarray, a: float) -> np.ndarray:
    """Rotates 2d vector by given angle."""
    x, y = vec
    return np.array([x * np.cos(a) - y * np.sin(a), x * np.sin(a) + y * np.cos(a)])


def cross_vec_to_mtx(vec: np.ndarray) -> np.ndarray:
    """Converts matrix of cross product to vector by which you have to cross-multiply to get same result."""
    return np.array([[0, -vec[2], vec[1]],
                     [vec[2], 0, -vec[0]],
                     [-vec[1], vec[0], 0]])


def cross_mtx_to_vec(mtx: np.ndarray) -> np.ndarray:
    """Converts matrix of cross product to vector by which you have to cross-multiply to get same result."""
    v = mtx[[[1, 0, 0], [2, 2, 1]]]
    v *= [-1, 1, -1]
    return v


def rotation_mtx_to_angles(r_mtx: np.ndarray, alternative=False) -> np.ndarray:
    """Converts between two forms of rotation representation"""
    assert r_mtx.shape == (3, 3)

    def f(x):
        if x > np.pi:
            return x - 2 * np.pi
        if x < -np.pi:
            return x + 2 * np.pi
        return x

    if np.abs(r_mtx[2, 0]) != 1.0:
        ay = -np.arcsin(r_mtx[2, 0])
        cs = np.cos(ay)
        ax = np.arctan2(r_mtx[2, 1] / cs, r_mtx[2, 2] / cs)
        az = np.arctan2(r_mtx[1, 0] / cs, r_mtx[0, 0] / cs)
        if not alternative:
            return np.array([f(x) for x in [ax, ay, az]])
        else:
            ay2 = np.pi - ay
            cs = np.cos(ay2)
            ax2 = np.arctan2(r_mtx[2, 1] / cs, r_mtx[2, 2] / cs)
            az2 = np.arctan2(r_mtx[1, 0] / cs, r_mtx[0, 0] / cs)
            return np.array([f(x) for x in [ax2, ay2, az2]])
    else:
        az = 0
        if r_mtx[2, 0] == -1.0:
            ay = np.pi / 2
            ax = az + np.arctan2(r_mtx[0, 1], r_mtx[0, 2])
        else:
            ay = -np.pi / 2
            ax = -az + np.arctan2(-r_mtx[0, 1], -r_mtx[0, 2])
        angles = np.array([ax, ay, az])
        return angles


def rotation_angles_to_mtx(angles: np.ndarray) -> np.ndarray:
    """ In Z Y X  order"""
    x, y, z = angles
    qx = np.identity(3)
    qx[[1, 2], [1, 2]] = np.cos(x)
    qx[1, 2] = -np.sin(x)
    qx[2, 1] = np.sin(x)
    qy = np.identity(3)
    qy[[0, 2], [0, 2]] = np.cos(y)
    qy[0, 2] = np.sin(y)
    qy[2, 0] = -np.sin(y)
    qz = np.identity(3)
    qz[[0, 1], [0, 1]] = np.cos(z)
    qz[0, 1] = -np.sin(z)
    qz[1, 0] = np.sin(z)
    return qz.dot(qy.dot(qx))


def left_quaternion_multiplication(q: np.ndarray) -> np.ndarray:
    return np.array([[-q[0], -q[1], -q[2], q[3]],
                     [q[3], -q[2], q[1], q[0]],
                     [q[2], q[3], -q[0], q[1]],
                     [-q[1], q[0], q[3], q[2]]])


def right_quaternion_multiplication(q: np.ndarray) -> np.ndarray:
    return np.array([[-q[0], -q[1], -q[2], q[3]],
                     [q[3], q[2], -q[1], q[0]],
                     [-q[2], q[3], q[0], q[1]],
                     [q[1], -q[0], q[3], q[2]]])


def estimate_relative_rotation(base_quats: List[Quaternion], relative_quats: List[Quaternion],
                               return_diagnostic: bool = False) -> \
        Union[Quaternion, Tuple[Quaternion, List[float]]]:
    """ Estimates relative rotation in transformation RELATIVE -> BASE """
    q, A = estimate_relative_rotation_with_matrix(base_quats, relative_quats)
    if return_diagnostic:
        residuals = []
        for i in range(len(base_quats)):
            residuals.append(np.linalg.norm(A[4 * i:4 * (i + 1), :].dot(np.ndarray(q))))
        return q, residuals
    else:
        return q


def estimate_relative_rotation_with_matrix(base_quats: List[Quaternion], relative_quats: List[Quaternion]) -> \
        Tuple[Quaternion, np.ndarray]:
    """ Estimates relative rotation in transformation RELATIVE -> BASE """
    k = len(base_quats)
    A = np.zeros((4 * k, 4))

    def fix_sign(q):
        if q[3] > 0:
            return np.array(q)
        else:
            return -np.array(q)

    for i, (rel_q, base_q) in enumerate(zip(map(fix_sign, relative_quats), map(fix_sign, base_quats))):
        A[4 * i:4 * (i + 1), :] = left_quaternion_multiplication(base_q) - right_quaternion_multiplication(rel_q)

    U, s, Vt = np.linalg.svd(A)
    return Quaternion(Vt[-1]), A


def estimate_relative_position(base_poses: List[np.ndarray], relative_poses: List[np.ndarray],
                               base_quats: List[Quaternion], relative_to_base_rotation: Quaternion,
                               return_diagnostic: bool = False) -> np.ndarray:
    """ Estimates RELATIVE -> BASE coordinate frame rotated with relative_to_basee_rotation
    (i.e. relative -> base ~ T = [R|t], where t is output from this function) """
    k = len(base_poses)
    A = np.zeros((3 * k, 4))
    b = np.zeros(3 * k)
    relative_to_base_rotation_mtx = relative_to_base_rotation.matrix33
    for i, (c0, c1, r0) in enumerate(zip(base_poses, relative_poses, base_quats)):
        A[3 * i:3 * (i + 1), :3] = np.eye(3) - Matrix33.from_quaternion(r0)
        A[3 * i:3 * (i + 1), 3] = relative_to_base_rotation_mtx.dot(c1)
        b[3 * i:3 * (i + 1)] = c0
    solution = np.linalg.lstsq(A, b, rcond=None)
    return solution[0]


def estimate_relative_position_with_matrix(base_poses: List[np.ndarray], relative_poses: List[np.ndarray],
                                           base_quats: List[Quaternion], relative_to_base_rotation: Quaternion):
    """ Estimates RELATIVE -> BASE coordinate frame rotated with relative_to_base_rotation
    (i.e. relative -> base ~ T = [R|t], where t is output from this function) """
    k = len(base_poses)
    A = np.zeros((3 * k, 4))
    b = np.zeros(3 * k)
    relative_to_base_rotation_mtx = relative_to_base_rotation.matrix33
    for i, (c0, c1, r0) in enumerate(zip(base_poses, relative_poses, base_quats)):
        A[3 * i:3 * (i + 1), :3] = np.eye(3) - Matrix33.from_quaternion(r0)
        A[3 * i:3 * (i + 1), 3] = relative_to_base_rotation_mtx.dot(c1)
        b[3 * i:3 * (i + 1)] = c0
    solution = np.linalg.lstsq(A, b, rcond=None)
    return solution[0], A


def full_transformation_mtx(translation, rotation):
    t = np.eye(4)
    mtx = rotation_angles_to_mtx(rotation)
    t[0:3, 0:3] = mtx
    t[0:3, 3] = -mtx.dot(translation)
    return t


def angles_to_quaternion(angles: np.ndarray) -> Quaternion:
    mtx = rotation_angles_to_mtx(angles)
    q = Quaternion.from_matrix(mtx)
    if q.w < 0:
        q = Quaternion(-q[0:4])
    return q


def quaternion_to_angles(q: Union[Quaternion, np.ndarray], alternative=False):
    if type(q) is np.ndarray:
        q = Quaternion(q)
    mtx = q.matrix33
    return rotation_mtx_to_angles(mtx, alternative)


def quaternion_to_angles2(q: Quaternion):
    t0 = 2.0 * (q.w * q.x + q.y * q.z)
    t1 = 1.0 - 2.0 * (q.x ** 2 + q.y ** 2)

    t2 = 2.0 * (q.w * q.y - q.z * q.x)
    t2 = np.clip(t2, -1, 1)

    t3 = 2.0 * (q.w * q.z + q.x * q.y)
    t4 = 1.0 - 2.0 * (q.y ** 2 + q.z ** 2)
    return np.array([atan2(t0, t1), asin(t2), atan2(t3, t4)])


def average_quaternion(qs: List[Quaternion]) -> Quaternion:
    # Found on https://github.com/christophhagen/averaging-quaternions/blob/master/averageQuaternions.py
    # Method derived in https://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/20070017872.pdf
    M = np.zeros((4, 4))
    for q in qs:
        M += np.outer(q, q)

    eigen_vals, eigen_vecs = np.linalg.eig(M)
    eigen_vecs = eigen_vecs[:, eigen_vals.argsort()]
    mean_quat_arr = np.real(eigen_vecs[:, -1])
    if mean_quat_arr[3] < 0:
        return Quaternion(-mean_quat_arr)
    else:
        return Quaternion(mean_quat_arr)


def geometric_median(X, eps=1e-5):
    # according for https://stackoverflow.com/questions/30299267/geometric-median-of-multidimensional-points
    y = np.mean(X, 0)

    while True:
        D = cdist(X, [y])
        nonzeros = (D != 0)[:, 0]

        Dinv = 1 / D[nonzeros]
        Dinvs = np.sum(Dinv)
        W = Dinv / Dinvs
        T = np.sum(W * X[nonzeros], 0)

        num_zeros = len(X) - np.sum(nonzeros)
        if num_zeros == 0:
            y1 = T
        elif num_zeros == len(X):
            return y
        else:
            R = (T - y) * Dinvs
            r = np.linalg.norm(R)
            rinv = 0 if r == 0 else num_zeros / r
            y1 = max(0, 1 - rinv) * T + min(1, rinv) * y

        if euclidean(y, y1) < eps:
            return y1

        y = y1
