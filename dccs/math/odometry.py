import os
import sys
from datetime import datetime
from typing import Callable, Optional, Tuple

import numpy as np
import time
from metrology import Metrology

from .camera_model import CameraData, CameraResizer
from .extrinsic import Transformation
from .filters import Movement3dModelBis
from ..config import orbslam_python_path
from ..utils.data import Ringbuffer
from ..utils.video_stream import VideoConsumer, VideoFeeder

sys.path.append(orbslam_python_path)
try:
    import orb_slam
except Exception as e:
    print("Cannot load orb_slam! " + str(e))


class SlamOdometry(VideoConsumer):
    TRACKING_BACK_MIN_TIME = 5
    POSITION_STABILITY_THRESHOLD = 0.02
    POSITION_BUFFER_SIZE = 500
    SLAM_VOCABULARY = os.path.join(orbslam_python_path,
                                   "../Vocabulary/ORBvoc.txt")
    TRACKING_LOST_THRESHOLD = 180
    VERBOSE_FLAG = 1
    ORB_SLAM_VISUAL_FLAG = 1 << 1
    STORE_POSE_LOG_FILE_FLAG = 1 << 2

    TIMESTAMP_POSITION = 0
    FILTER_STATE_POSITION = 1
    FILTER_ERRORS_POSITIONS = FILTER_STATE_POSITION + Movement3dModelBis.STATE_SIZE
    RAW_POSITION = FILTER_ERRORS_POSITIONS + Movement3dModelBis.ERRORS_SIZE
    ENTRY_LENGTH = RAW_POSITION + Transformation.INLINED_QUATERNION_SIZE

    def __init__(self, video_feeder: VideoFeeder,
                 notify_callback: Callable[[Transformation, float], None],
                 camera_data: CameraData, flags: int = None, **kwargs):
        super(SlamOdometry, self).__init__(video_feeder)
        self.resizer = CameraResizer(camera_data.get_base_camera(), 2, True)
        camera_data.set_yaml_values(self.resizer.dist_focal,
                                    self.resizer.dist_principal_point)

        self.send_update_callback = notify_callback
        self.position_buffer = Ringbuffer(SlamOdometry.POSITION_BUFFER_SIZE,
                                          SlamOdometry.ENTRY_LENGTH)
        self.tracking_periods = Ringbuffer(
            SlamOdometry.POSITION_BUFFER_SIZE // 2, 2)
        self.tracking_lost_time = 0
        self.last_position_time = 0
        self.flags = flags

        self.frames_counter = Metrology.meter('odom-frames')
        self.filter = Movement3dModelBis()
        orb_slam.init(SlamOdometry.SLAM_VOCABULARY, camera_data.get_yaml_path())
        if self._is_set(SlamOdometry.STORE_POSE_LOG_FILE_FLAG):
            base_name = "full_positions_log"
            self.pose_log_file = camera_data.get_file("logs",
                                                      base_name + datetime.now().strftime(
                                                          "%Y%m%d_%H%M"), "wb")
        self.frames_combo = 0
        self.position = None

    def print_state(self):
        print(" T: {} P: {}".format("NA" if self.tracking_lost_time else "OK",
                                    self.position), end="\r")

    def process(self, frame: np.ndarray) -> bool:
        frame_time = time.time()
        resized_frame = self.resizer.process(frame)
        raw_pos = orb_slam.track(resized_frame,
                                 frame_time)  # returns full, 4x4 pose
        # Note, that it consist rotation and then translation, so last row of pose equals R^(-1)*C
        self.frames_counter.mark()
        if raw_pos is not None:
            if self.tracking_lost_time is not None:
                self.tracking_lost_time = None
                self.tracking_periods.add(np.array([frame_time, np.inf]))
            self.position = Transformation.from_full(raw_pos)
            self.update_position(self.position, frame_time)
        else:
            if self.tracking_lost_time is None:
                self.tracking_lost_time = frame_time
                self.tracking_periods.get_entry()[1] = frame_time
            if frame_time - self.tracking_lost_time > SlamOdometry.TRACKING_LOST_THRESHOLD:
                self.tracking_lost_time = frame_time

        self.print_state()
        return True

    @property
    def tracking_back_time(self):
        return self.tracking_periods.get_entry()[0]

    def update_position(self, position: Transformation,
                        timestamp: float) -> None:
        # Insert to ringbuffer
        dt = timestamp - self.last_position_time
        self.last_position_time = timestamp
        # TODO: filter on quaternion too
        self.filter.update(position.inline, dt)
        self.check_and_run_callback(position, timestamp)
        with self.position_buffer.lock:
            SlamOdometry.compose_position_entry(self.position_buffer.row,
                                                timestamp,
                                                position.inline_quaternion,
                                                self.filter.last_error,
                                                self.filter.get_state())
            if self._is_set(SlamOdometry.STORE_POSE_LOG_FILE_FLAG):
                self.pose_log_file.write(self.position_buffer.row.tobytes())
            self.position_buffer.inc()

    def check_and_run_callback(self, position: Transformation,
                               timestamp: float) -> None:
        velocity = self.filter.velocity
        # print("Velocity: ", velocity, "Projection error: ", self.filter.last_error)
        threshold_met = np.all(np.abs(self.filter.last_error) < SlamOdometry.POSITION_STABILITY_THRESHOLD)
        #threshold_met &= np.all(np.abs(self.filter.velocity) < SlamOdometry.POSITION_STABILITY_THRESHOLD)
        # np.all(np.abs(velocity) < SlamOdometry.POSITION_STABILITY_THRESHOLD) and np.all(
        if threshold_met and timestamp - self.tracking_back_time > SlamOdometry.TRACKING_BACK_MIN_TIME:
            self.send_update_callback(position, timestamp)

    def _get_bound_for_time(self, timestamp: float) -> Tuple[int, int]:
        including_range_index = self.tracking_periods.get_last_before(timestamp)
        if including_range_index is None:
            return None
        including_range = self.tracking_periods.get_entry(including_range_index)
        if including_range is None or including_range[0] > timestamp or \
                including_range[1] < timestamp:
            # timestamp not in tracked time
            return None

        # Lowest SHIFT that have time bigger than timestamp. Its means how many entries was created after this entry!
        last_before_shift = self.position_buffer.get_last_before(timestamp)
        if last_before_shift is None or last_before_shift == 0:
            return None
        else:
            first_after_shift = last_before_shift - 1
        return last_before_shift, first_after_shift

    def get_position_by_time(self, timestamp: float) -> Optional[Transformation]:
        bounds = self._get_bound_for_time(timestamp)
        if bounds is None:
            return None
        last_before_shift, first_after_shift = bounds
        first_after_entry = self.position_buffer.get_entry(first_after_shift)
        last_before_entry = self.position_buffer.get_entry(last_before_shift)
        first_after_time = SlamOdometry.get_time_from_entry(first_after_entry)
        last_before_time = SlamOdometry.get_time_from_entry(last_before_entry)
        dt = first_after_time - last_before_time
        assert dt != 0
        first_after_position = Transformation.from_inlined(
            SlamOdometry.get_filtered_from_entry(first_after_entry))
        last_before_position = Transformation.from_inlined(
            SlamOdometry.get_filtered_from_entry(last_before_entry))
        last_before_weight = (first_after_time - timestamp) / dt
        return Transformation.interpolate(last_before_position,
                                          first_after_position,
                                          last_before_weight)

    def get_position_with_threshold(self, timestamp: float, treshold: float):
        bounds = self._get_bound_for_time(timestamp)
        if bounds is None:
            return None
        last_before_shift, first_after_shift = bounds
        first_after_entry = self.position_buffer.get_entry(first_after_shift)
        last_before_entry = self.position_buffer.get_entry(last_before_shift)

        # first_v = SlamOdometry.get_velocity_from_entry(first_after_entry)
        # last_v = SlamOdometry.get_velocity_from_entry(last_before_entry)
        # if np.any(np.abs(first_v) > treshold) or np.any(np.abs(last_v) > treshold):
        #    return None

        first_err = SlamOdometry.get_filter_errors_from_entry(first_after_entry)
        last_err = SlamOdometry.get_filter_errors_from_entry(last_before_entry)
        if np.any(np.abs(first_err) > treshold) or np.any(
                np.abs(last_err) > treshold):
            return None

        first_after_time = SlamOdometry.get_time_from_entry(first_after_entry)
        last_before_time = SlamOdometry.get_time_from_entry(last_before_entry)
        dt = first_after_time - last_before_time
        assert dt != 0
        first_after_position = Transformation.from_inlined(
            SlamOdometry.get_filtered_from_entry(first_after_entry))
        last_before_position = Transformation.from_inlined(
            SlamOdometry.get_filtered_from_entry(last_before_entry))
        last_before_weight = (first_after_time - timestamp) / dt
        return Transformation.interpolate(last_before_position,
                                          first_after_position,
                                          last_before_weight)

    @staticmethod
    def get_velocity_from_entry(entry: np.ndarray) -> np.ndarray:
        return \
            entry[SlamOdometry.FILTER_STATE_POSITION:
                  SlamOdometry.FILTER_STATE_POSITION + Movement3dModelBis.STATE_SIZE][
                Movement3dModelBis.VELOCITY_INDICES]

    @staticmethod
    def get_filter_errors_from_entry(entry: np.ndarray) -> np.ndarray:
        return entry[
               SlamOdometry.FILTER_ERRORS_POSITIONS: SlamOdometry.FILTER_ERRORS_POSITIONS + Movement3dModelBis.ERRORS_SIZE]

    @staticmethod
    def get_raw_from_entry(entry: np.ndarray) -> np.ndarray:
        return entry[SlamOdometry.RAW_POSITION: SlamOdometry.RAW_POSITION + Transformation.INLINED_QUATERNION_SIZE]

    @staticmethod
    def get_filtered_from_entry(entry: np.ndarray) -> np.ndarray:
        return np.append(entry[SlamOdometry.FILTER_STATE_POSITION: SlamOdometry.FILTER_STATE_POSITION + 3],
                         entry[SlamOdometry.FILTER_STATE_POSITION + 6: SlamOdometry.FILTER_STATE_POSITION + 9])

    @staticmethod
    def get_time_from_entry(entry: np.ndarray) -> float:
        return entry[SlamOdometry.TIMESTAMP_POSITION]

    @staticmethod
    def compose_position_entry(entry: np.ndarray, timestamp: float,
                               raw_position: np.ndarray,
                               filter_errors: np.ndarray,
                               filter_state: np.ndarray) -> None:
        entry[SlamOdometry.TIMESTAMP_POSITION] = timestamp
        entry[SlamOdometry.FILTER_STATE_POSITION:
              SlamOdometry.FILTER_STATE_POSITION + Movement3dModelBis.STATE_SIZE] = filter_state
        entry[SlamOdometry.FILTER_ERRORS_POSITIONS:
              SlamOdometry.FILTER_ERRORS_POSITIONS + Movement3dModelBis.ERRORS_SIZE] = filter_errors
        entry[SlamOdometry.RAW_POSITION:
              SlamOdometry.RAW_POSITION + Transformation.INLINED_QUATERNION_SIZE] = raw_position

    def get_buffer_pos(self):
        return self.position_buffer.index

    def get_position_range(self, last_pos, pos):
        if last_pos == pos:
            return np.zeros((0, SlamOdometry.ENTRY_LENGTH))
        return self.position_buffer.get_entries(last_pos, pos)

    def get_current_timestamp(self):
        return SlamOdometry.get_time_from_entry(
            self.position_buffer.get_entry())

    def _is_set(self, flag: int) -> bool:
        return (self.flags & flag) != 0

