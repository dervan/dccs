import functools
import logging
import os
from collections import defaultdict
from datetime import datetime
from multiprocessing.pool import Pool
from typing import Dict, Optional

import cv2
import numpy as np

from dccs.math.algebra import rotation_mtx_to_angles
from dccs.math.camera_model import CameraData, CameraModel
from dccs.utils.images import load_bgr, gray, cv_show, fit_photos
from .extrinsic import Transformation, estimate_relation


def pose_is_close(local: Transformation, remote: Transformation):
    return all(np.abs(local.inline_quaternion - remote.inline_quaternion)
               < PoseBasedCalibrator.CLOSE_ORIENTATION_TRESHOLD)


class PoseBasedCalibrator:
    ESTIMATIONS_FREQUENCY_RATIO = 3
    MIN_POSITIONS_COUNT = 5
    CLOSE_ORIENTATION_TRESHOLD = 0.1

    def __init__(self, camera_data: CameraData, store_logs=False) -> None:
        self.camera_name = camera_data.name
        self.positions_registry = defaultdict(list)
        self.raw_estimations: Dict[str, Transformation] = {}
        self.last_fitness = 0

        self.store_logs = store_logs
        if store_logs:
            filename_root = "_log" + self.camera_name + "_"
            filename_root = filename_root.replace("/", "_") + datetime.now().strftime("%Y%m%d_%H%M")
            logs_path = camera_data.path_to("logs")
            estimations_path = os.path.join(logs_path, "estimations" + filename_root)
            positions_path = os.path.join(logs_path, "positions" + filename_root)
            self.estimations_log = open(estimations_path, "w+", buffering=1)
            self.positions_log = open(positions_path, "w+", buffering=1)
        self.estimators_pool = Pool(4)

    ##############################################
    # Logging part
    ##############################################
    @staticmethod
    def estimation_log_entry(idx: int, relative_pose: Transformation, pose_fit):
        return "base: {}, pose({}) t:{} r:{} \n".format(idx, pose_fit, relative_pose.translation, relative_pose.angles)

    @staticmethod
    def position_log_entry(remote_camera_name, remote_position, remote_time) -> str:
        return "{};{};{}\n".format(remote_camera_name,
                                   ','.join([str(r) for r in remote_position]), remote_time)

    def log_estimation(self, log_string):
        if self.store_logs:
            self.estimations_log.write(log_string)

    def log_position(self, log_string):
        if self.store_logs:
            self.positions_log.write(log_string)

    def add_position(self, remote_camera_name: str, remote: Transformation, local: Transformation, remote_time: float):
        """
        Insert new measurement of relation between base camera and remote camera. If new estimation was retrieived based
        on provided measurement it is returned - otherwise it returns None.

        :param remote_camera_name:
        :param remote:
        :param local:
        :param remote_time:
        :return: REMOTE -> BASE Transformation!
        """

        self.log_position(
            PoseBasedCalibrator.position_log_entry(remote_camera_name, remote.inline_quaternion, remote_time))
        self.log_position(
            PoseBasedCalibrator.position_log_entry(self.camera_name, local.inline_quaternion, remote_time))

        # Verify if it's not too close
        if any([pose_is_close(x[1], remote) for x in self.positions_registry[remote_camera_name]]):
            logging.warning("Position %s is too close to existing positions!", str(remote.inline_quaternion))
            return

        self.positions_registry[remote_camera_name].append((remote_time, remote, local))
        pos_count = len(self.positions_registry[remote_camera_name])
        avg_estimation = None
        if PoseBasedCalibrator.should_run_calibration(pos_count):
            _, remote_positions, local_positions = zip(*self.positions_registry.get(remote_camera_name))
            estimations = self.estimators_pool.map(
                functools.partial(PoseBasedCalibrator.estimate_relation,
                                  remote_positions=remote_positions, local_positions=local_positions), range(pos_count))
            for base_idx, estimation in enumerate(estimations):
                logging.info("B: {} Est: {}".format(base_idx, estimation))
            avg_estimation = Transformation.average_transformation(estimations)
            logging.info("Avg: Est: {}".format(avg_estimation))
            if avg_estimation is not None:
                self.raw_estimations[remote_camera_name] = avg_estimation
                self.log_estimation(
                    PoseBasedCalibrator.position_log_entry(remote_camera_name, avg_estimation.inline_full, remote_time))
        return avg_estimation

    def log_not_matched(self, remote_name, remote, remote_time):
        self.log_position(PoseBasedCalibrator.position_log_entry(remote_name, remote.inline_quaternion, remote_time))
        self.log_position(self.camera_name + "; None\n")

    @staticmethod
    def estimate_relation(base_idx: int, remote_positions, local_positions) -> Transformation:
        correction_remote = remote_positions[base_idx]
        correction_local = local_positions[base_idx]

        corrected_remote_positions = [q.substract(correction_remote) for q in remote_positions]
        corrected_local_positions = [q.substract(correction_local) for q in local_positions]

        return estimate_relation(corrected_local_positions, corrected_remote_positions)

    @staticmethod
    def should_run_calibration(positions_count: int) -> bool:
        return positions_count > PoseBasedCalibrator.MIN_POSITIONS_COUNT and \
               positions_count % PoseBasedCalibrator.ESTIMATIONS_FREQUENCY_RATIO == 0


class StereoCalibrator:
    DEFAULT_TERM = (cv2.TERM_CRITERIA_MAX_ITER + cv2.TERM_CRITERIA_EPS, 100000, 0.001)

    def __init__(self, base_camera_data: CameraData, slave_camera_data: CameraData, chessboard_shape=None):
        self.termination_criteria = CameraCalibrator.DEFAULT_TERM
        self.base_camera = base_camera_data
        self.slave_camera = slave_camera_data

        self.chessboard_shape = chessboard_shape or CameraCalibrator.DEFAULT_CHESSBOARD
        self.chessboard_points = np.zeros((self.chessboard_shape[0] * self.chessboard_shape[1], 3), np.float32)
        self.chessboard_points[:, :2] = np.mgrid[0:self.chessboard_shape[0], 0:self.chessboard_shape[1]].T.reshape(-1,
                                                                                                                   2)
        self.relation = None

    def images(self):
        base_images = sorted(os.listdir(self.base_camera.path_to("stereo_photos")))
        slave_images = sorted(os.listdir(self.slave_camera.path_to("stereo_photos")))
        return zip(base_images, slave_images)

    def calibrate(self):
        world_points = []
        base_points = []
        slave_points = []

        base_cam = self.base_camera.get_base_camera()
        slave_cam = self.slave_camera.get_base_camera()

        for b, s in self.images():
            if b != s:
                print(b, "!=", s)
            else:
                print(b, "==", s)
                found = self.find_chessboards(b)
                if found is None:
                    print("Chessboard not found!")
                    continue

                # CHESSBOARDS HAVE TO HAVE SAME ORIENTATION!!
                found_base_points, found_slave_points = found
                world_points.append(self.chessboard_points)
                base_points.append(found_base_points)
                slave_points.append(found_slave_points)

        retval, cameraMatrix1, distCoeffs1, cameraMatrix2, distCoeffs2, R, T, E, F = cv2.stereoCalibrate(
            world_points, base_points, slave_points,
            base_cam.intr_mtx, base_cam.distortion,
            slave_cam.intr_mtx, slave_cam.distortion,
            self.base_camera.xy_shape,
            criteria=(cv2.TERM_CRITERIA_EPS, 1000, 1e-8),
            flags=cv2.CALIB_RATIONAL_MODEL | cv2.CALIB_FIX_INTRINSIC)
        print("Quality:\n", retval)
        print("R:\n", R)
        print("ang:\n", rotation_mtx_to_angles(R))
        print("T:\n", T)
        print("RT:\n", R.dot(T))
        print("R^-1T:\n", R.T.dot(T))
        print("F:\n", F)
        t = R.T.dot(T).ravel()
        full = np.eye(4)
        full[0:3, 0:3] = R
        full[0:3, -1] = t
        self.relation = (R, T)
        return self.relation

    def find_chessboards(self, filename, debug=False):
        base_photo = load_bgr(self.base_camera.path_to("stereo_photos/" + filename))
        slave_photo = load_bgr(self.slave_camera.path_to("stereo_photos/" + filename))
        if base_photo is None:
            print("There is no photo  ", self.base_camera.name, filename)
            return
        if slave_photo is None:
            print("There is no photo  ", self.slave_camera.name, filename)
            return

        ret, base_corners = cv2.findChessboardCorners(base_photo, self.chessboard_shape)
        if not ret:
            print("Pattern not found on ", self.base_camera.name, filename)
            return

        ret, slave_corners = cv2.findChessboardCorners(slave_photo, self.chessboard_shape)
        if not ret:
            print("Pattern not found on ", self.slave_camera.name, filename)
            return

        base_gray = gray(base_photo)
        slave_gray = gray(slave_photo)

        cv2.cornerSubPix(base_gray, base_corners, (11, 11), (-1, -1), self.termination_criteria)
        cv2.cornerSubPix(slave_gray, slave_corners, (11, 11), (-1, -1), self.termination_criteria)

        # verify if detected chessboards have the same orientation
        base_chessboard_orientation = base_corners[0] - base_corners[-1]
        slave_chessboard_orientation = slave_corners[0] - slave_corners[-1]

        # and if they have different orientation, reverse order (what is same as detecting rotated board)
        if base_chessboard_orientation.dot(slave_chessboard_orientation.T) < 0:
            slave_corners = slave_corners[-1::-1]

        return base_corners, slave_corners

    def show_debug(self):
        R, T = self.relation

        base_cam = self.base_camera.get_base_camera()
        slave_cam = self.slave_camera.get_base_camera()

        for photo_name, _ in self.images():
            # Unfortunately stereoRectify does not work well with rational model
            base_photo = base_cam.undistort(load_bgr(self.base_camera.path_to("stereo_photos/" + photo_name)))
            slave_photo = slave_cam.undistort(load_bgr(self.slave_camera.path_to("stereo_photos/" + photo_name)))

            result = cv2.stereoRectify(base_cam.undistorted.intr_mtx, np.zeros(8), slave_cam.undistorted.intr_mtx,
                                       np.zeros(8),
                                       base_cam.xy_shape, R=R, T=T, alpha=1)

            rotation1, rotation2, new_mtx1, new_mtx2, Q, roi1, roi2 = result
            map1x, map1y = cv2.initUndistortRectifyMap(base_cam.undistorted.intr_mtx, np.zeros(8), rotation1, new_mtx1,
                                                       base_cam.xy_shape, cv2.CV_32FC1)
            map2x, map2y = cv2.initUndistortRectifyMap(slave_cam.undistorted.intr_mtx, np.zeros(8), rotation2, new_mtx2,
                                                       slave_cam.xy_shape, cv2.CV_32FC2)
            mapped_base = cv2.remap(base_photo, map1x, map1y, cv2.INTER_CUBIC)
            mapped_slave = cv2.remap(slave_photo, map2x, map2y, cv2.INTER_CUBIC)
            cv_show(fit_photos([mapped_base, mapped_slave], target_width=1920), max_width=1920)
        # cv2.waitKey(0)
        # save_cameras([Transformation.identity(), self.relation],
        #             "/tmp/calib_{0}_{1}.obj".format(self.base_camera.sname, self.slave_camera.sname))
        # print(self.relation)


class CameraCalibrator:
    """This class is used to generate calibrated camera basing on some world points and corresponding
    image points (e.g. from calibration grid)"""
    DEFAULT_TERM = (cv2.TERM_CRITERIA_EPS, 0, 0.00000001)
    DEFAULT_CHESSBOARD = (7, 9)

    def __init__(self, camera_data: CameraData, termination_criteria=None):
        self.termination_criteria = termination_criteria or CameraCalibrator.DEFAULT_TERM
        self.camera_data = camera_data
        self.yx_shape = camera_data.yx_shape
        self.world_points = []
        self.image_points = []
        self.Rvec = None
        self.Tvec = None
        self.calibrated = False
        self.camera = None
        print("New camera calibrator with resolution ", self.yx_shape)
        chessboard_shape = CameraCalibrator.DEFAULT_CHESSBOARD
        self.chessboard_shape = chessboard_shape
        self.chessboard_points = np.zeros((chessboard_shape[0] * chessboard_shape[1], 3), np.float32)
        self.chessboard_points[:, :2] = np.mgrid[0:chessboard_shape[0], 0:chessboard_shape[1]].T.reshape(-1, 2)

    @property
    def xy_shape(self):
        y, x = self.yx_shape
        return x, y

    def append_points_from_photo(self, filename, debug=False):
        photo = load_bgr(filename)
        gray_photo = gray(photo)

        ret, corners = cv2.findChessboardCorners(photo, self.chessboard_shape)
        print(filename, ret)
        if ret:
            self.world_points.append(self.chessboard_points)

            cv2.cornerSubPix(gray_photo, corners, (11, 11), (-1, -1),
                             self.termination_criteria)
            self.image_points.append(corners)

            if debug:
                # Draw and display the corners
                cv2.drawChessboardCorners(photo, self.chessboard_shape, corners,
                                          ret)
                cv_show(photo, max_width=1920, time=1000)
        return ret

    def find_points_on_photos(self, debug=False):
        for filename in self.camera_data.photos():
            self.append_points_from_photo(filename, debug)

    def calibrate(self, debug=False):
        self.find_points_on_photos(debug)
        if len(self.world_points) == 0:
            print("Cannot calibrate camera properly! Check if photos are correct!")
            self.set_dummy()
        else:
            self.generate_camera()
        print("Camera data saved!")

    def generate_camera(self, alpha=0, fix_ratio=False) -> CameraModel:
        """When worldPoints and imagePoints are initialized we can run calibration routine. Returns new
        calibrated camera."""
        flags = cv2.CALIB_RATIONAL_MODEL
        if fix_ratio:
            flags |= cv2.CALIB_FIX_ASPECT_RATIO
        ret, C, K, self.Rvec, self.Tvec = \
            cv2.calibrateCamera(self.world_points,
                                self.image_points,
                                self.xy_shape,
                                np.eye(3),
                                None,
                                flags=flags,
                                criteria=self.termination_criteria)
        Cp, roi = cv2.getOptimalNewCameraMatrix(C, K, self.xy_shape, alpha)
        print("Camera matrix:\n", C)
        print("Calibrated distortion:\n", K)
        camera = CameraModel(*self.xy_shape)
        camera.set_camera_matrix(C)
        camera.set_distortion_camera_matrix(Cp)
        camera.set_distortion(K)
        self.camera = camera
        return camera

    def dump(self, path: Optional[str] = None) -> None:
        """Saves proper data in numpy txt dumps."""

        if path is None:
            path = self.camera_data.path_to('data')
        if not os.path.exists(path):
            os.mkdir(path)
        np.savetxt(os.path.join(path, "intrinsic.mtx"), self.camera.intr_mtx)
        if self.camera.distortion is not None:
            np.savetxt(os.path.join(path, "distortion.mtx"),
                       self.camera.distortion)
            np.savetxt(os.path.join(path, "intr_distortion.mtx"),
                       self.camera.undistorted.intr_mtx)

    '''
    self.cyl_maps = np.meshgrid(np.arange(self.img_shape[0], dtype='f4'), np.arange(self.img_shape[1], dtype='f4'))
    '''

    def set_dummy(self):
        self.camera = CameraModel(*self.xy_shape)

    def print_debug_stats(self):
        mean_error = 0
        max_err = 0
        for image_point, world_point, rvec, tvec in zip(self.image_points,
                                                        self.world_points,
                                                        self.Rvec, self.Tvec):
            imgpoints2, _ = cv2.projectPoints(world_point, rvec, tvec,
                                              self.camera.intr_mtx,
                                              self.camera.distortion)
            error = cv2.norm(image_point, imgpoints2, cv2.NORM_L2) / len(
                imgpoints2)
            mean_error += error
            max_err = max([max_err, error])

        print("Mean error: {}, max error: {}".format(
            mean_error / len(self.world_points), max_err))
