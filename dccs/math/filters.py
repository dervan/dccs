import bisect
from typing import TypeVar

import numpy as np
from numpy.linalg import LinAlgError
from pyrr import Quaternion

from . import algebra


class MeanModel:
    class MeanCalc:
        def __init__(self, x):
            self.values = [x]

        def add(self, x):
            bisect.insort(self.values, x)

        @property
        def len(self):
            return len(self.values)

        def mean(self):
            if self.len % 2:
                return self.values[self.len // 2]
            else:
                return sum(self.values[self.len // 2:self.len // 2 + 2]) / 2

    def __init__(self, initial_state: np.ndarray):
        self.means = [MeanModel.MeanCalc(x) for x in initial_state]

    def update(self, measurement: np.ndarray, dt: float):
        for x, m in zip(measurement, self.means):
            m.add(x)
        return self.get_state()

    def get_state(self):
        return [m.mean() for m in self.means]

    @property
    def ndim(self):
        return len(self.means)


class FilteredModel:
    def __init__(self, initial_state: np.ndarray, measurement_size: int = None, dt: float = 1.0, g: float = 1.0,
                 h: float = 1.0):
        self.state = initial_state
        self.diff = np.zeros_like(initial_state)
        if measurement_size is None:
            self.mdim = len(self.state)
        else:
            self.mdim = measurement_size
        self.g = g
        self.h = h

    @property
    def ndim(self):
        return len(self.state)

    def update(self, measurement: np.ndarray, dt: float):
        assert len(measurement) == self.mdim
        predicted_state = self.state + self.diff * dt
        residual = measurement - predicted_state
        self.diff = self.diff + self.h * residual / dt
        self.state = predicted_state + self.g * residual
        return self.state

    def get_state(self):
        return self.state


class KalmanFilter(object):
    """
    According to: https://github.com/rlabbe/filterpy/blob/master/filterpy/kalman/kalman_filter.py
    """

    def __init__(self, dim_x, dim_z):
        self.dim_x = dim_x
        self.dim_z = dim_z
        self.x = np.zeros((dim_x, 1))
        self.y = np.zeros((dim_z, 1))
        self.z = np.zeros((dim_z, 1))
        self.P = np.eye(dim_x)
        self.Q = np.eye(dim_x)
        self.F = np.eye(dim_x)
        self.H = np.zeros((dim_z, dim_x))
        self.R = np.eye(dim_z)
        self.M = np.zeros((dim_z, dim_z))
        self.K = np.zeros((dim_x, dim_x))
        self.S = np.zeros((dim_z, dim_z))
        self.SI = np.zeros((dim_z, dim_z))

        self._I = np.eye(dim_x)
        self.x_prior = self.x.copy()
        self.P_prior = self.P.copy()
        self.x_post = self.x.copy()
        self.P_post = self.P.copy()

    def predict(self):
        self.x = np.dot(self.F, self.x)
        self.P = np.dot(np.dot(self.F, self.P), self.F.T) + self.Q
        self.x_prior = self.x.copy()
        self.P_prior = self.P.copy()

    def update(self, z):
        self.y = z - np.dot(self.H, self.x)
        PHT = np.dot(self.P, self.H.T)
        self.S = np.dot(self.H, PHT) + self.R
        self.SI = np.linalg.inv(self.S)
        self.K = np.dot(PHT, self.SI)

        self.x = self.x + np.dot(self.K, self.y)
        I_KH = self._I - np.dot(self.K, self.H)
        self.P = np.dot(np.dot(I_KH, self.P), I_KH.T) + np.dot(np.dot(self.K, self.R), self.K.T)
        self.z = z.copy()

        self.x_post = self.x.copy()
        self.P_post = self.P.copy()


T = TypeVar('T')


class UnscentedKalmanFilter:
    def __init__(self, model_dim, state_dim, measure_dim, q=0.01, r=0.01):
        self.model_dim = model_dim
        self.state_dim = state_dim
        self.measure_dim = measure_dim
        self.x = np.zeros((state_dim,))
        self.x_prediction = np.zeros((state_dim,))
        self.z_prediction = np.zeros((measure_dim,))
        self.P = np.eye(model_dim) * 0.1
        self.P_prediction = np.eye(model_dim)
        self.P_xz = np.zeros((model_dim, measure_dim))
        self.P_measure = np.eye(measure_dim)
        self.samples_count = 2 * model_dim + 1
        # also known as X
        self.sigma_states = np.zeros((self.samples_count, state_dim))
        # also known as Y
        self.predicted_states = np.zeros((self.samples_count, state_dim))
        # also known as Z
        self.predicted_measurements = np.zeros((self.samples_count, measure_dim))
        # also known as W'
        self.diff_to_mean = np.zeros((self.samples_count, model_dim))

        self.R = np.eye(measure_dim) * r
        self.Q = np.eye(model_dim) * q
        self.alpha = 0.001
        self.beta = 2.0
        self.kappa = model_dim
        # self.lambda_ = self.alpha ** 2 * (model_dim + self.kappa) - model_dim
        self.sigma_weight = 0.5 / (model_dim + self.kappa)
        self.central_mean_weight = self.kappa / (model_dim + self.kappa)
        self.central_cov_weight = self.central_mean_weight
        # self.central_cov_weight = self.lambda_ / (model_dim + self.lambda_) + (1 - self.alpha ** 2 + self.beta)

    def get_state(self):
        return self.x

    def sigma_points(self):
        """
        :returns set of 2 * model_dim sigma points, every with size of model_dim
        """
        import scipy
        try:
            S = scipy.linalg.cholesky((self.P + self.Q) * (self.model_dim + self.kappa))
        except LinAlgError:
            print("Incorrect P matrix: ")
            print(self.P + self.Q)
            print(np.linalg.det(self.P + self.Q))
            raise
        return np.vstack((S, -S))

    def prepare_disturbed_states(self):
        sp = self.sigma_points()
        assert len(sp) == self.model_dim * 2
        for i, state_disturbance in enumerate(sp):
            self.sigma_states[i] = self.apply_model_diff(self.x, state_disturbance)
        self.sigma_states[-1] = self.x
        return self.sigma_states

    def transform_states(self, dt):
        for i in range(self.samples_count):
            self.predicted_states[i] = self.process_state(self.sigma_states[i], dt)
            # print("Prediction: ", self.sigma_states[i], "=>", self.predicted_states[i])
            self.predicted_measurements[i] = self.measurement_from_state(self.predicted_states[i])

    def compute_covariances(self):
        self.P_prediction = np.zeros((self.model_dim, self.model_dim))
        self.P_xz = np.zeros((self.model_dim, self.measure_dim))
        self.P_measure = np.zeros((self.measure_dim, self.measure_dim))
        for i in range(self.samples_count - 1):
            wi = self.diff_to_mean[i]
            zi = self.zdiff(self.predicted_measurements[i], self.z_prediction)
            print(i, wi, zi)
            self.P_prediction += self.sigma_weight * np.outer(wi, wi)
            self.P_xz += self.sigma_weight * np.outer(wi, zi)
            self.P_measure += self.sigma_weight * np.outer(zi, zi)

        wc = self.diff_to_mean[-1]
        zc = self.zdiff(self.predicted_measurements[-1], self.z_prediction)
        self.P_prediction += self.central_cov_weight * np.outer(wc, wc)
        self.P_xz += self.central_cov_weight * np.outer(wc, zc)
        self.P_measure += self.central_cov_weight * np.outer(zc, zc)

        """
        print("P_prediction:")
        print(self.P_prediction)
        print("P_xz:")
        print(self.P_xz)
        print("P_measure:")
        print(self.P_measure)
        """

    def predict(self, dt):
        self.prepare_disturbed_states()
        self.transform_states(dt)
        self.calculate_means()
        self.compute_covariances()

    def update(self, measurement):
        P_vv = self.P_measure + self.R
        gain = self.P_xz.dot(np.linalg.inv(P_vv))

        print("P_xz", self.P_xz)
        print("gain", gain)
        self.x = self.updated_state(gain, measurement)
        self.P = self.P_prediction - gain.dot(P_vv).dot(gain.T)

    def add(self, measurement, dt):
        self.predict(dt)
        self.update(measurement)
        return self.x

    def calculate_means(self):
        raise NotImplementedError

    def process_state(self, state: T, dt) -> T:
        raise NotImplementedError

    def measurement_from_state(self, state: T):
        raise NotImplementedError

    def apply_model_diff(self, state, model_diff) -> T:
        raise NotImplementedError

    def updated_state(self, gain, measurement):
        raise NotImplementedError

    def zdiff(self, a, b):
        raise NotImplementedError


class QuaternionFilter(UnscentedKalmanFilter):
    MEAN_CALC_THRESHOLD = 0.01

    def __init__(self):
        super().__init__(6, 7, 4, q=0.001, r=0.0001)
        self.x[3] = 1.0
        self.P *= 0.01

    @property
    def velocity(self):
        return self.x[4:].ravel()

    @property
    def last_error(self):
        return (self.x[:4] - self.x_prediction[:4]).ravel()

    def calculate_means(self):
        M = np.zeros((4, 4))
        for i in range(self.samples_count - 1):
            qi = self.predicted_states[i, 0:4]
            M += np.outer(qi, qi)
        M *= self.sigma_weight

        qc = self.predicted_states[-1, 0:4]
        M += self.central_mean_weight * np.outer(qc, qc)

        eigen_vals, eigen_vecs = np.linalg.eig(M)
        eigen_vecs = eigen_vecs[:, eigen_vals.argsort()]
        mean_quat_arr = np.real(eigen_vecs[:, -1])
        if mean_quat_arr[3] < 0:
            mean_quat = Quaternion(-mean_quat_arr)
        else:
            mean_quat = Quaternion(mean_quat_arr)

        predicted_velocity = self.central_mean_weight * self.predicted_states[-1][4:]
        predicted_angles = self.central_mean_weight * self.measurement_from_state(self.predicted_states[-1])
        for i in range(self.samples_count - 1):
            predicted_velocity += self.sigma_weight * self.predicted_states[i][4:]
            predicted_angles += self.sigma_weight * self.measurement_from_state(self.predicted_states[i])

        # print("Mean quat_ang:", self.measurement_from_state(mean_quat))
        # print("Mean ang:", predicted_angles)
        print("Predicted v:", predicted_velocity)
        # Recalculate last samples, to save diff to mean
        for i in range(self.samples_count):
            error_quat = Quaternion(self.predicted_states[i][0:4]).cross(mean_quat.inverse)
            self.diff_to_mean[i][0:3] = algebra.quaternion_to_angles(error_quat)
            self.diff_to_mean[i][3:] = self.predicted_states[i][4:] - predicted_velocity

        self.x_prediction = np.zeros(7)
        self.x_prediction[:4] = mean_quat
        self.x_prediction[4:] = predicted_velocity
        # TODO: Check that!
        self.z_prediction = self.measurement_from_state(
            self.x_prediction)  # sum([diff[0:3] for diff in self.diff_to_mean])/self.samples_count
        # self.z_prediction = algebra.quaternion_to_angles(mean_quat) #sum([diff[0:3] for diff in self.diff_to_mean])/self.samples_count
        # self.z_prediction = sum([diff[0:3] for diff in self.diff_to_mean])/self.samples_count

    def calculate_means2(self):
        predicted_velocity = sum([state[4:] for state in self.predicted_states]) / self.samples_count
        print("Mean v:", predicted_velocity)
        mean_quat = Quaternion(self.x[0:4])
        correction_quat = algebra.angles_to_quaternion([1, 0, 0])
        cnt_max = 100
        step_size = 1
        last_mean_angle = 100
        # Iteratively find a mean
        while correction_quat.angle > QuaternionFilter.MEAN_CALC_THRESHOLD and cnt_max > 0:
            cnt_max -= 1
            mean_error_angles = np.zeros(3)
            mean_angle = 0
            for i in range(self.samples_count):
                error_quat = Quaternion(self.predicted_states[i][0:4]).cross(mean_quat.inverse)
                # print("Mean + ", algebra.quaternion_to_angles(error_quat), error_quat)
                mean_error_angles += algebra.quaternion_to_angles(error_quat)
                mean_angle += 2 * np.arccos(error_quat.w)

            mean_angle /= self.samples_count
            mean_error_angles /= self.samples_count
            mean_error_angles *= step_size
            # print("Mean = ", mean_error_angles, mean_angle)
            if last_mean_angle > mean_angle:
                last_mean_angle = mean_angle
            else:
                step_size *= 0.5
            correction_quat = algebra.angles_to_quaternion(mean_error_angles)
            mean_quat = correction_quat.cross(mean_quat)

        # Recalculate last samples, to save diff to mean
        z_avg = self.central_mean_weight * self.measurement_from_state(self.predicted_states[-1])
        for i in range(self.samples_count - 1):
            z_avg += self.sigma_weight * self.measurement_from_state(self.predicted_states[i])

        for i in range(self.samples_count):
            error_quat = Quaternion(self.predicted_states[i][0:4]).cross(mean_quat.inverse)
            self.diff_to_mean[i][0:3] = algebra.quaternion_to_angles(error_quat)
            self.diff_to_mean[i][3:] = self.predicted_states[i][4:] - predicted_velocity
        self.x_prediction = np.zeros(7)
        self.x_prediction[:4] = mean_quat
        self.x_prediction[4:] = predicted_velocity
        self.z_prediction = z_avg

    def apply_model_diff(self, state, model_diff):
        noise_quat = algebra.angles_to_quaternion(model_diff[0:3])
        noised = np.zeros(7)
        noised[0:4] = Quaternion(state[0:4]).cross(noise_quat)
        noised[4:] = state[4:] + model_diff[3:]
        return noised

    def process_state(self, state: np.ndarray, dt) -> np.ndarray:
        new_state = np.zeros(self.state_dim)
        quat = Quaternion(state[0:4])
        dquat = algebra.angles_to_quaternion(state[4:] * dt)
        new_state[0:4] = quat.cross(dquat)
        new_state[4:] = state[4:]
        return new_state

    def measurement_from_state(self, state: np.ndarray) -> Quaternion:
        return state[0:4]

    def updated_state(self, gain, measurement: Quaternion):
        diff = self.zdiff(measurement.__array__(), self.z_prediction)
        return self.apply_model_diff(self.x_prediction, gain.dot(diff))

    def get_angles(self):
        return self.measurement_from_state(self.x)

    def zdiff(self, a, b):
        if a[3] < 0:
            a *= -1
        if b[3] < 0:
            b *= -1
        return a - b


class Movement3dModel:
    def __init__(self, model_certainty=3, measurement_certainty=0.1):
        # Model have 3d position + 3d rotation + 3d velocity + 3d angular velocity
        # Measurement have only position and velocity.
        self._filter = KalmanFilter(12, 6)
        self._filter.H = np.zeros((6, 12))
        self._filter.H[[0, 1, 2, 3, 4, 5], [0, 1, 2, 3, 4, 5]] = 1
        self._filter.F = np.eye(12)
        self._filter.R = np.eye(6) * model_certainty
        self._filter.Q = np.eye(12) * measurement_certainty

    def _set_time_diff(self, dt):
        self._filter.F = np.eye(12)
        self._filter.F[0:3, 6:9] += np.eye(3) * dt
        self._filter.F[3:6, 9:] += np.eye(3) * dt

    def get_state(self):
        return self._filter.x

    def update(self, z, dt):
        self._set_time_diff(dt)
        self._filter.predict()
        self._filter.update(z.reshape((6, 1)))
        return self._filter.H.dot(self.get_state())

    @property
    def last_error(self):
        return self._filter.y

    @property
    def velocity(self):
        return self._filter.x[[3, 4, 5, 9, 10, 11]]


class Position3dFilter:
    def __init__(self, model_uncertainty=3, measurement_uncertainty=0.1):
        # Model have 3d position + 3d rotation + 3d velocity + 3d angular velocity
        # Measurement have only position and velocity.
        self._pos_kf = KalmanFilter(6, 3)
        self._pos_kf.H = np.zeros((3, 6))
        self._pos_kf.H[[0, 1, 2], [0, 1, 2]] = 1
        self._pos_kf.F = np.eye(6)
        self._pos_kf.R = np.eye(3) * model_uncertainty
        self._pos_kf.Q = np.eye(6) * measurement_uncertainty

    def _set_time_diff(self, dt):
        self._pos_kf.F = np.eye(6)
        self._pos_kf.F[0:3, 3:6] += np.eye(3) * dt

    def get_state(self):
        return self._pos_kf.x.ravel()

    def update(self, z, dt):
        self._set_time_diff(dt)
        self._pos_kf.predict()
        self._pos_kf.update(z.reshape((3, 1)))
        return self._pos_kf.H.dot(self.get_state())

    @property
    def last_error(self):
        return self._pos_kf.y.ravel()

    @property
    def velocity(self):
        return self._pos_kf.x[[3, 4, 5]].ravel()


class Movement3dModelQuat:
    def __init__(self):
        self.pos_filter = Position3dFilter()
        self.quat_filter = QuaternionFilter()

    def get_state(self):
        return np.concatenate((self.pos_filter.get_state(), self.quat_filter.get_state()))

    def update(self, z, dt):
        assert len(z) == 7
        self.pos_filter.update(z[0:3], dt)
        self.quat_filter.add(z[3:], dt)
        return np.concatenate(
            (self.pos_filter.get_state()[0:3], algebra.quaternion_to_angles(self.quat_filter.get_state()[0:4])))

    @property
    def last_error(self):
        return np.concatenate((self.pos_filter.last_error, self.quat_filter.last_error))

    @property
    def velocity(self):
        return np.concatenate((self.pos_filter.velocity, self.quat_filter.velocity))


class Movement3dModelBis:
    STATE_SIZE = 12
    ERRORS_SIZE = 6
    VELOCITY_INDICES = [3, 4, 5, 9, 10, 11]

    def __init__(self):
        self.pos_filter = Position3dFilter()
        self.angle_filter = Position3dFilter()

    def get_state(self):
        return np.concatenate((self.pos_filter.get_state(), self.angle_filter.get_state()))

    def update(self, z, dt):
        self.pos_filter.update(z[0:3], dt)
        self.angle_filter.update(z[3:], dt)

    @property
    def last_error(self):
        return np.concatenate((self.pos_filter.last_error, self.angle_filter.last_error))

    @property
    def velocity(self):
        return np.concatenate((self.pos_filter.velocity, self.angle_filter.velocity))

    def get_position_state(self):
        return self.pos_filter.get_state()
