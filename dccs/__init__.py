import aiomas

from .utils.aiomas import create_container, prepare_shutdown_hooks


def run_dccs(container):
    prepare_shutdown_hooks()
    aiomas.run()
    container.shutdown()


__all__ = [create_container, run_dccs]
