import os
import sys

project_root = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))

camera_data_root = os.path.join(project_root, '/home/drv/pCloudDrive/magisterka/camera_data')
orbslam_python_path = os.path.join(project_root, '../ORB_SLAM2/lib')

def camera_data_dir(camera_name, subdir=None):
    if subdir is None:
        return os.path.join(camera_data_root, camera_name)
    else:
        return os.path.join(camera_data_root, camera_name, subdir)
