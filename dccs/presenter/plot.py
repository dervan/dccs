import threading
from typing import List, Callable

import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt


def plot_image(img, fig_size=(22, 22)):
    """Shows image in matplotlib"""
    plt.figure(figsize=(fig_size[0], fig_size[1]))
    plt.imshow(img)
    plt.show()


def live_plot(data: np.ndarray, range_accessor: Callable, plot_arrangement: list, data_lock: threading.Lock):
    """
    :param data: Time-varying array with data
    :param range_accessor: function which returns array with indices first and last row in data buffer
    :param range: Time-varying counter of important row (pos 1) and index of first position in buffer (pos 0)
            Should be np.array to be passed by reference.
    :param plot_arrangement: list of indices, one for every column.
            columns with same name will be showed on same plot.
            Special value "t" means that this column will be data on 0x axis.
    :return:
    """
    # Note: Contains import to be thread-safe
    import matplotlib.pyplot as plt
    import matplotlib.animation as animation
    import numpy as np
    plots = list(set(plot_arrangement) - set("t"))
    plots.sort(key=lambda x: plot_arrangement.index(x))
    plots.append("t")
    plots_idx = [plots.index(x) for x in plot_arrangement]
    xdata = plot_arrangement.index("t")

    fig, axs = plt.subplots(len(plots) - 1, 1, sharex=True, sharey=False)
    fig.tight_layout(pad=0.05)

    def update(i):
        data_range = range_accessor()
        [ax.clear() for ax in axs]
        for ax, name in zip(axs, plots):
            ax.set_title(name)
        with data_lock:
            shifted_data = np.roll(data[:data_range[1], :], -data_range[0], axis=0)
        for data_index, plot_index in enumerate(plots_idx):
            if data_index != xdata:
                axs[plot_index].plot(shifted_data[:, xdata], shifted_data[:, data_index])

    a = animation.FuncAnimation(fig, update, interval=200)
    plt.show()


class LiveRingbufferPlotter(threading.Thread):
    def __init__(self, data_reference: np.ndarray, range_source: Callable, labels: List[str],
                 data_lock: threading.Lock):
        super(LiveRingbufferPlotter, self).__init__(name='plotter')
        self.data = data_reference
        self.range_source = range_source
        self.labels = labels
        self.lock = data_lock

    def run(self):
        live_plot(self.data, self.range_source, self.labels, self.lock)
