import numpy as np

from ..math.algebra import l2_norm

_camera_object = (np.array([[0, 0, 0],
                            [0.5, 0.5, 1],
                            [-0.5, 0.5, 1],
                            [-0.5, -0.5, 1],
                            [0.5, -0.5, 1],
                            [0, 0.6, 1]]).astype('f4'),
                  np.array([[0, 1, 2], [0, 2, 3], [0, 3, 4], [0, 4, 1], [1, 2, 5]], dtype='int32'))


def save_cameras(transformations_list, filepath_out):
    vertices = np.ndarray((0, 3))
    indices = np.ndarray((0, 3))

    scale = 1.0
    for t in transformations_list:
        for t2 in transformations_list:
            if t != t2:
                scale = min(scale, 0.5 * l2_norm(t.substract(t2).translation))

    print("scale:", scale)
    camera_vertices = _camera_object[0] * scale

    for t in transformations_list:
        points: np.ndarray = t.transform_points(camera_vertices)
        local_indices = _camera_object[1] + vertices.shape[0] + 1
        indices = np.append(indices, local_indices, axis=0)
        vertices = np.append(vertices, points, axis=0)

    with open(filepath_out, 'w') as ofile:
        begin = "OBJ File: \n"
        ofile.write(begin)

        for v in vertices:
            # line = "v {v.x} {v.y} {v.z} " for full precision
            line = "v {:.4f} {:.4f} {:.4f} ".format(v[0], v[1], v[2])
            ofile.write(line + "\n")

        for f in indices:
            #line = "{0} {1} "
            # obj vertex indices for faces start at 1 not 0 like blender.
            line = "f " + ' '.join([str(int(s)) for s in f])
            ofile.write(line + "\n")