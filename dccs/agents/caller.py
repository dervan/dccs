from typing import List, Tuple
import aiomas
import asyncio
import numpy as np

from ..utils.data import dccs_serializers


class Caller(aiomas.Agent):
    address_schema = "tcp://{}:{}/0"

    @staticmethod
    def address(ip: str, port: int):
        return Caller.address_schema.format(ip, port)

    base_container = None
    refcount = 0
    port = 5555
    data = None

    def __init__(self, target: str, port: int = None) -> None:
        """Caller instance is created per target. Target should be given in full URL form."""
        if Caller.base_container is None:
            Caller.init_container()
        Caller.refcount += 1
        super().__init__(Caller.base_container)

        if not target.startswith("tcp:"):
            assert port is not None
            target = Caller.address(target, port)
        print("New caller: ", target)
        self.proxy = aiomas.run(self.container.connect(target))
        print("Connected")
        self.id = target.__hash__()
        self.target = target
        self.port = port
        # def handler(signum, frame):
        #    print('Signal handler called with signal', signum)

        # Set the signal handler and a 5-second alarm
        # signal.signal(signal.SIGINT, handler)

    @property
    def full_address(self):
        return self.target, self.port

    @staticmethod
    def shutdown() -> None:
        """Notifies that this caller will not be used"""
        Caller.refcount -= 1
        if Caller.refcount == 0:
            print("Remove Caller container!")
            Caller.base_container.shutdown()
            Caller.base_container = None

    @staticmethod
    def init_container() -> None:
        """Creates new container if is not created. Binds to any free port."""
        while Caller.base_container is None:
            Caller.port += 1
            try:
                Caller.base_container = aiomas.Container.create(('', Caller.port), codec=aiomas.MsgPack,
                                                                extra_serializers=dccs_serializers)
                print("New Caller container on port ", Caller.port)
            except OSError as oe:
                if oe.errno != 98:
                    raise


class RemotePosition(Caller):
    def __init__(self, name, target, port=None):
        super().__init__(target, port)
        self.name = name

    async def run(self, value_consumer):
        while True:
            print("running")
            poses = await self.proxy.get_remote_poses(self.name)
            if len(poses) > 0:
                print("Consumer")
                value_consumer(self.full_address, poses[0])
            else:
                print("No pose :(")
            await asyncio.sleep(3)


class PositionRetreiver:
    def __init__(self, target_addresses: List[Tuple[str, str, int]]):
        self.remote_cameras = []
        for target in target_addresses:
            self.remote_cameras.append(RemotePosition(*target))

    @staticmethod
    def print(address, pose):
        print("Pose from", address, ":", pose)

    def run(self):
        loop = asyncio.get_event_loop()
        for remote in self.remote_cameras:
            asyncio.run_coroutine_threadsafe(remote.run(PositionRetreiver.print), loop)


class RemoteCaller(Caller):
    """This function allows to call some functions on remote agent"""

    def __init__(self, target: str) -> None:
        super(RemoteCaller, self).__init__(target)

    async def get_camera_list(self) -> List[str]:
        """Tests getting camera list"""
        cam_list = await self.proxy.get_camera_list()
        print("Camera list:", cam_list)
        return cam_list

    async def get_stream(self, target_ip, target_port):
        """Tests getting video stream"""
        print("Get stream to : ", target_ip, target_port)
        returned = await self.proxy.get_stream(target_ip, target_port)
        print("Returned:", returned)
        return returned

    async def get_intrinsic(self) -> np.ndarray:
        """Takes intrinsic from camera driver or server"""
        intr = await self.proxy.get_intrinsic()
        print("Intrinsic:\n", intr)
        return intr

    async def get_image_raw(self) -> np.ndarray:
        """Takes raw image from camera driver"""
        img = await self.proxy.get_image_raw()
        if img is not None:
            print("Image shape:", img.shape)
        else:
            print("None img")
        return img

    async def get_image(self, name: str) -> np.ndarray:
        """Takes image from server"""
        img = await self.get_image(name)
        if img is not None:
            print("Image shape:", img.shape)
        else:
            print("None img")
        return img

    async def get_position_log_continous(self):
        _debug_float_format = "{: .4f}"
        while True:
            pos_diff = await self.proxy.get_pos_diff()
            for i, line in enumerate(pos_diff):
                print(" ".join([_debug_float_format.format(n) for n in line]), end='')
                if i == len(pos_diff) - 1:
                    print(" <=")
                else:
                    print()
            await asyncio.sleep(3)

    async def get_estimations_continous(self):
        np.set_printoptions(precision=3)
        while True:
            poses = await self.proxy.get_remote_poses()
            print(poses)
            print("=" * 50)
            await asyncio.sleep(3)
