import asyncio
import sys

import numpy as np

from ..agents.caller import Caller
from ..math.algebra import quaternion_to_angles
from ..math.odometry import SlamOdometry
from ..presenter.plot import LiveRingbufferPlotter
from ..utils.data import Ringbuffer


class Plotter(Caller):
    INDICES = [0, 1, 2, 3, 7, 8, 9, 19, 20, 21]

    def __init__(self, target: str, log_file_name: str = None) -> None:
        super(Plotter, self).__init__(target)
        self.position_buffer = Ringbuffer(SlamOdometry.POSITION_BUFFER_SIZE, 25)
        pos_labels = ["t",
                      "x", "x", "x", "xerr", "yerr", "zerr",
                      "q", "q", "q", "qerr", "werr", "eerr",
                      "xerr", "yerr", "zerr", "qerr", "werr", "eerr",
                      "x", "x", "x", "q", "q", "q"]
        plotter = LiveRingbufferPlotter(self.position_buffer.data,
                                        self.position_buffer.get_data_range,
                                        pos_labels,
                                        self.position_buffer.lock)
        plotter.start()
        if log_file_name:
            self.log_file = open(log_file_name, "w+", buffering=1)
        else:
            self.log_file = None
        loop = asyncio.get_event_loop()
        asyncio.run_coroutine_threadsafe(self.download(), loop)

    async def download(self):
        _debug_float_format = "{: .4f}"
        np.set_printoptions(4)
        while True:
            pos_diff = await self.proxy.get_pos_diff()
            try:
                print(len(pos_diff), "new lines")
                allstr = np.array([_debug_float_format.format(p) for p in pos_diff[-1]])
                print(" ".join(allstr[[4, 5, 6, 10, 11, 12, 13, 14, 15, 16, 17]]))
                with self.position_buffer.lock:
                    for line in pos_diff:
                        self.position_buffer.row[:-3] = line[:-4]
                        self.position_buffer.row[-3:] = quaternion_to_angles(line[-4:])
                        self.position_buffer.inc()
                        if self.log_file:
                            self.log_file.write(";".join([_debug_float_format.format(x) for x in line]))
                            self.log_file.write("\n")
            except Exception:
                print(sys.exc_info())

            await asyncio.sleep(3)
