from time import sleep, time

import aiomas
import asyncio
import logging
from metrology import Metrology
from typing import List, Dict

from ..math.calibrators import PoseBasedCalibrator
from ..math.camera_model import CameraData
from ..math.extrinsic import Transformation
from ..math.odometry import SlamOdometry
from ..utils.images import gray
from ..utils.video_stream import VideoFeeder, RawPresenter, VideoSenderConsumer


class CameraDriverServer(aiomas.Agent):
    """ This class contains part which is dependent on single camera.
    It's bridge between main logic and hardware. It also distributes video feed to
    other submodules like odometry or presenter."""

    _UPDATE_LIMIT_SECONDS = 0.5

    def __init__(self, container: aiomas.Container, camera_name: str, remote_cameras_addresses: List[str] = None,
                 preview_enabled: bool = False,
                 odometry_enabled: bool = False,
                 odometry_visual_mode: bool = False):
        super().__init__(container)
        # Load saved camera data
        self.camera_name = camera_name
        self.cam_data = CameraData(camera_name)
        self.remote_cameras = {}
        self.updates_meter = Metrology.meter('remote_updates')

        self.last_update_time = 0
        loop = asyncio.get_event_loop()
        asyncio.run_coroutine_threadsafe(self.connect_to_remote(remote_cameras_addresses or []), loop)

        if self.cam_data.type == 'v4l':
            self.video_feeder = VideoFeeder(self.cam_data)
        else:
            raise NotImplementedError

        self.video_feeder.start()
        sleep(1)

        if preview_enabled:
            self.presenter = RawPresenter(self.video_feeder, name=camera_name)
            self.presenter.start()
        else:
            self.presenter = None

        if odometry_enabled:
            self.odom = SlamOdometry(self.video_feeder, self.send_update, self.cam_data,
                                     SlamOdometry.STORE_POSE_LOG_FILE_FLAG |
                                     (SlamOdometry.ORB_SLAM_VISUAL_FLAG if odometry_visual_mode else 0),
                                     camera_name=camera_name)
            self.odom.start()
            self.calibrator = PoseBasedCalibrator(self.cam_data, store_logs=True)
        else:
            self.odom = None
            self.calibrator: PoseBasedCalibrator = None
        self.last_pos_request = 0

    async def connect_to_remote(self, camera_addresses: List[str]) -> None:
        """This is practically part of init, but run in asyncio"""
        for addr in camera_addresses:
            logging.debug("Connecting to remote camera " + addr)
            remote_camera = await self.container.connect(addr)
            remote_name = await remote_camera.get_name()
            logging.info("Connected to {}!".format(remote_name))
            remote_camera.register_camera(self.camera_name, self)
            self.remote_cameras[remote_name] = remote_camera

    @property
    def camera_model(self):
        return self.cam_data.get_base_camera()

    def send_update(self, position: Transformation, position_time: float) -> None:
        actual_time = time()
        if actual_time - self.last_update_time < CameraDriverServer._UPDATE_LIMIT_SECONDS:
            return
        else:
            self.last_update_time = actual_time
        current_remotes = list(self.remote_cameras.values())

        async def _update_coro():
            for remote in current_remotes:
                await remote.register_remote_position(self.camera_name, position, position_time)

        result = asyncio.run_coroutine_threadsafe(_update_coro(), self.container.loop)
        result.result()
        self.updates_meter.mark()

    def teardown(self):
        self.video_feeder.stop()
        if self.presenter:
            self.presenter.stop()
        if self.odom:
            self.odom.stop()

    def get_image(self):
        """Takes a frame from video feed"""
        frame = self.video_feeder.get_frame()
        return frame

    #####################
    # Part of exposed API
    #####################

    @aiomas.expose
    def get_name(self):
        return self.camera_name

    @aiomas.expose
    def register_camera(self, remote_camera_name, remote_camera_proxy):
        if remote_camera_name in self.remote_cameras:
            logging.info("Already registered: {}".format(remote_camera_name))
        else:
            logging.info("Remotely registered new camera: {}".format(remote_camera_name))
            self.remote_cameras[remote_camera_name] = remote_camera_proxy

    @aiomas.expose
    def register_remote_position(self, remote_camera_name, remote_position: Transformation, remote_time):
        logging.debug("Update from {} on time {} with position\n{}"
                      .format(remote_camera_name, remote_time, remote_position))

        if remote_time > self.odom.get_current_timestamp():
            logging.info("sleeping...")
            sleep(0.3)
        local_position: Transformation = self.odom.get_position_with_threshold(remote_time, 0.1)
        if local_position is not None:
            logging.info("Update from {} received!".format(remote_camera_name))
            self.calibrator.add_position(remote_camera_name, remote_position, local_position, remote_time)
        else:
            logging.info("Update from {} received, but there is no good matching pose!".format(remote_camera_name))
            self.calibrator.log_not_matched(remote_camera_name, remote_position, remote_time)
        return 1

    @aiomas.expose
    def get_image_raw(self):
        """ Function to retreive single image from camera. Should not be used in real processing because it's horribly slow """
        return self.get_image()

    @aiomas.expose
    def get_image_gray(self):
        """ Returns gray raw image """
        return gray(self.get_image())

    @aiomas.expose
    def get_image_rect(self):
        """ Returns rectified image """
        img = self.get_image()
        return self.camera_model.undistort(img)

    @aiomas.expose
    def get_stream(self, target_ip, target_port):
        sender = VideoSenderConsumer(self.video_feeder, target_ip, target_port)
        sender.start()
        return True

    @aiomas.expose
    def get_intrinsic(self):
        """ Returns intrinsic parameters """
        return self.camera_model.intr_mtx

    @aiomas.expose
    def get_pos_diff(self):
        last_pos = self.last_pos_request
        pos = self.odom.get_buffer_pos()
        self.last_pos_request = pos
        logging.info("Pos request" + str(pos) + " " + str(last_pos))
        return self.odom.get_position_range(last_pos, pos)

    @aiomas.expose
    def get_remote_pose(self, camera_name: str = None) -> Transformation:
        logging.info("Remote pose for {} request", camera_name)
        if self.calibrator is not None:
            poses = self.calibrator.get_estimations()
            if camera_name is None:
                raise ValueError("Camera name not provided!")
            else:
                return poses.get(camera_name)

    @aiomas.expose
    def get_remote_poses(self) -> Dict[str, Transformation]:
        logging.info("Remote poses request")
        if self.calibrator is not None:
            return self.calibrator.get_estimations()
