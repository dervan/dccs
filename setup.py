from setuptools import setup, find_packages

setup(name='dccs',
      version='0.1',
      description='Distributed Camera Calibration System',
      long_description='System for calibration cameras based solely on their movement.',
      classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.6',
        'Topic :: Text Processing :: Linguistic',
      ],
      keywords='distributed camera calibration',
      url='http://bitbucket.org/dervan/dccs.git',
      author='Michał Jagielski',
      author_email='michal@jagielski.net',
      license='MIT',
      packages=find_packages(),
      install_requires=[
        'pyrr',
        'aiomas',
        'asyncio',
        'opencv-python',
        'matplotlib',
        'numpy',
        'metrology',
        'scipy'
      ],
      include_package_data=True,
      zip_safe=False)
