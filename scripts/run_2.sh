#!/bin/bash
SESSION="session_two"
tmux new -s $SESSION -d
tmux split-window -v -t $SESSION
FLAGS="-o -p"
#tmux send-keys -t $SESSION:0.0 'python3 server.py' C-m
tmux send-keys -t $SESSION:0.0 "python3 ../bin/daemon.py final_stereo/straight 9991 $FLAGS -c tcp://localhost:9992/0" C-m
tmux send-keys -t $SESSION:0.1 "python3 ../bin/daemon.py final_stereo/askew 9992 $FLAGS -c tcp://localhost:9991/0" C-m
tmux select-layout even-vertical
tmux attach -t $SESSION

