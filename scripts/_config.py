import sys

sys.path.append("..")

def error(text, *args):
    print(text.format(*args), file=sys.stderr)
