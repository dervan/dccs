import argparse
import asyncio

import _config
from dccs import run_dccs
from dccs.agents.caller import RemoteCaller

actions = {
    "estimations": lambda caller: caller.get_estimations_continous(),
    "poslog": lambda caller: caller.get_position_log_continous(),
    "stream": lambda caller: caller.get_stream("192.168.13.13", "5000")
}

parser = argparse.ArgumentParser()

parser.add_argument("--address", "-a", help="Address of remote camera.", default="localhost")
parser.add_argument("--port", "-p", type=int, help="Port on remote", default="9992")

parser.add_argument("--action", "-c", help="Name of action to perform. Possible values: " + str(actions.keys()))
args = parser.parse_args()

remote = RemoteCaller("tcp://{}:{}/0".format(args.address, args.port))
loop = asyncio.get_event_loop()

asyncio.run_coroutine_threadsafe(actions[args.action](remote), loop)
run_dccs(remote.container)
