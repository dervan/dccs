import argparse

import aiomas
import cv2

import _config
from dccs.utils.images import cv_show, fit_photos
from dccs.agents import RemoteCaller
from dccs.math.camera_model import CameraData
from dccs.math.calibrators import CameraCalibrator
from dccs.utils.video_stream import VideoStream

parser = argparse.ArgumentParser()

parser.add_argument("--local-camera", "-l", help="Directory containing needed config for local camera")
parser.add_argument("--address", "-a", help="Address of remote camera. It cannot be specified together with -l",
                    default="localhost")
parser.add_argument("--port", "-p", type=int, help="Port on which deamon is started")

parser.add_argument("--calib", "-c", action='store_true',
                    help="Runs calibration photos capture mode. Cannot be specified with -s")

parser.add_argument("--output", "-o", help="Name of captured photo")
parser.add_argument("--width", "-w", type=int, default=1920, help="Screen width")
parser.add_argument("--mode", "-m", help="Capture mode", default="raw")
parser.add_argument("--stereo", "-s", help="Capture stereo", default=False, action="store_true")
args = parser.parse_args()


def start_capture(opts):
    video_streams = []
    for cam_name in opts.local_camera.split(","):
        camera_data = CameraData(cam_name)
        video_streams.append(VideoStream(camera_data, opts.mode))

    is_stereo = opts.stereo
    if is_stereo:
        assert len(video_streams) == 2, "Stereo can be specified only for two cameras!"
    while True:
        images = []
        for video in video_streams:
            if video.device.isOpened():
                _, frame = video.read()
                images.append(frame)

        if len(images) == 0:
            print("No images to present!")
            exit(1)

        scaled_frame = fit_photos(images, opts.width)

        cv2.imshow("Frames", scaled_frame)
        key = cv2.waitKey(10)
        if key == 32:
            for video in video_streams:
                if is_stereo:
                    video.save_stereo()
                else:
                    video.save()
        elif key == 27:
            for video in video_streams:
                video.release()
            cv2.destroyAllWindows()
            exit(0)


def start_capture_with_calib(opts):
    if len(opts.local_camera.split(",")) > 1:
        print("Calib can take only one argument")
        exit(1)

    cam_name = opts.local_camera
    camera_data = CameraData(cam_name)
    if camera_data.xres == 0 or camera_data.yres == 0:
        print("Zero image size, camera configuration is corrupted!")
        exit(1)

    video = VideoStream(camera_data, opts.mode)
    camera = camera_data.get_base_camera()
    fix_ratio = False
    alpha = 0

    calibrator = CameraCalibrator(camera_data)
    while True:
        if video.device.isOpened():
            _, frame = video.read()
            images = fit_photos([frame, camera.undistort(frame)], opts.width)
            cv2.imshow("Frames", images)
        key = cv2.waitKey(10)
        recompute = False
        if key == 32:
            filename = video.save()
            ret = calibrator.append_points_from_photo(filename)
            if ret:
                print("Found new corners!")
            recompute = True
        elif key == 82:
            alpha += 0.05
            recompute = True
        elif key == 84:
            alpha -= 0.05
            recompute = True
        elif key == 102:  # f
            fix_ratio = not fix_ratio
            recompute = True
        elif key == 100:
            calibrator.dump()
            print("Camera data saved!")
        elif key == 27:
            video.release()
            cv2.destroyAllWindows()
            exit(1)

        if recompute:
            calibrator.generate_camera(alpha, fix_ratio)
            camera = calibrator.camera
            print("Got calibration:", camera.distortion)
            print("Alpha: ", alpha, "Fixed ratio:", fix_ratio)
            calibrator.print_debug_stats()


if args.local_camera:
    if args.calib:
        start_capture_with_calib(args)
    else:
        start_capture(args)
else:
    caller_proxy = RemoteCaller("tcp://{}:{}/0".format(args.address, args.port))
    if args.mode == "raw":
        photo = aiomas.run(caller_proxy.get_image_raw())
    else:
        raise NotImplemented

    if args.output:
        cv2.imwrite(args.output, photo)
    else:
        cv_show(photo)
    caller_proxy.shutdown()
