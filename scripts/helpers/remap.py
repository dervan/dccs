import sys

import cv2
import numpy as np


sys.path.append("..")
from dccs.utils.images import cv_show
from dccs.math.camera_model import CameraData

img = cv2.imread(sys.argv[1])
cam_data = CameraData(sys.argv[2])
camera_model = cam_data.get_base_camera()

remapped = camera_model.undistort(img)

print(img.shape, remapped.shape)
cv_show(remapped)
cv2.imwrite("remapped_" + sys.argv[1], remapped)
