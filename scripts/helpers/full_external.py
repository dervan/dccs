import argparse
import logging
import sys

import matplotlib.pyplot as plt
import numpy as np

sys.path.append("..")
from dccs.math.extrinsic import Transformation

sys.path.append("..")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Script used to visualise camera trajectories")
    parser.add_argument("positions_files", nargs='*', help="Files with serialized positions.")
    args = parser.parse_args()
    logging.basicConfig(level=logging.DEBUG)

    start_time = 0
    timestamps = []
    #for filename in args.positions_files:
    #    with open(filename) as positions:
    #        startline = np.array(positions.readline().split(","), dtype='f8')
    #        start_time = max(start_time, startline[0])
    #    timestamps.append([])

    for filename in args.positions_files:
        ts = []
        xs = []
        with open(filename) as positions:
            for entry in positions.readlines():
                full_entry = np.array(entry.split(","), dtype='f8')
                ts.append(full_entry[0])
                xs.append(0)
        plt.plot(ts, xs, marker='.', linestyle='none')

    plt.show()
