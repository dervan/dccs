import argparse
import logging
import sys

import numpy as np

sys.path.append("..")
from dccs.math.calibrators import PoseBasedCalibrator
from dccs.math.extrinsic import Transformation
from dccs.presenter.model import save_cameras

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Script used to verify which external relation would be calculated for given positions")
    parser.add_argument("positions_file", help="File with serialized positions.")
    parser.add_argument("base_name", help="Name of base camera")
    parser.add_argument("-d", "--dump", help="File to dump estimated rig model", default=None, required=False)
    args = parser.parse_args()
    logging.basicConfig(level=logging.DEBUG)

    with open(args.positions_file) as positions:
        base_name = args.base_name
        calibrator = PoseBasedCalibrator(base_name)
        remote = None
        local = None
        for entry in positions.readlines():
            entry_data = entry.split(";")
            if len(entry_data) != 3:
                print("Missing position!")
                continue
            name, position_string, timestamp = entry_data
            position = np.array(position_string.split(","), dtype='f8')
            if name != base_name:
                remote = name, position, timestamp
            else:
                local = name, position, timestamp
            if not (remote is None or local is None):
                # fig = plt.figure()
                # ax = fig.add_subplot(111, projection='3d')
                estimation = calibrator.add_position(remote[0], remote[1], local[1], remote[2])
                # q1 = Quaternion(remote[1][3:])
                # q2 = Quaternion(local[1][3:])
                # r1 = q1.matrix33.dot(unit)
                # r2 = q2.matrix33.dot(unit)
                # ax.plot([0, r1[0]], [0, r1[1]], [0, r1[2]])
                # ax.plot([0, r2[0]], [0, r2[1]], [0, r2[2]])
                # plt.show()
                # print(" ".join(str(n) for n in estimation.inline_full) if estimation else "")
                print("=" * 80)
                remote = None
                local = None

    if args.dump:
        poses = [Transformation.identity()]
        for k, v in calibrator.get_estimations().items():
            poses.append(v)
        save_cameras(poses, args.dump)
