import argparse
import logging
import sys

import matplotlib.pyplot as plt
import numpy as np

sys.path.append("..")
from dccs.math.extrinsic import Transformation

sys.path.append("..")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Script used to verify which external relation would be calculated for given positions")
    parser.add_argument("positions_file", help="File with serialized positions.")
    parser.add_argument("indices", nargs='*', help="Files with serialized positions.")
    args = parser.parse_args()

    indices_list = [[int(i) for i in indices_string.split(",")] for indices_string in args.indices]

    fig, axs = plt.subplots(len(args.indices), 1, sharex=True, sharey=False)
    fig.tight_layout()

    xsss = [[[] for _ in indices] for indices in indices_list]
    ys = []

    with open(args.positions_file) as positions:
        for entry in positions.readlines():
            full_entry = np.array(entry.split(","), dtype='f8')
            for xss, indices in zip(xsss, indices_list):
                for xs, idx in zip(xss, indices):
                    xs.append(full_entry[idx])
            ys.append(full_entry[0])

    for ax, xss in zip(axs, xsss):
        for xs in xss:
            ax.plot(ys, xs)

    plt.show()
