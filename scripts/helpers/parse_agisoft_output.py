import argparse

import numpy as np
import sys

import xmltodict

sys.path.append("..")
from dccs.math.extrinsic import Transformation
from dccs.presenter.model import save_cameras


def str_to_transform(s):
    return Transformation.from_full(np.array(s.split(), dtype='f4').reshape((4, 4)))


parsers = argparse.ArgumentParser()
parsers.add_argument("-x", "--xml", help='File with agisoft photoscan output')
parsers.add_argument("-c", "--cameras", help='Cameras in calibration')

args = parsers.parse_args()
with open(args.xml, 'r') as xmlfile:
    xmldict = xmltodict.parse(xmlfile.read())

camera_names = [c.replace("/", "_") for c in args.cameras.split(",")]
cameras = {}

for c in xmldict['document']['chunk']['cameras']['camera']:
    transform = c.get('transform')
    if transform:
        cameras[c['@label']] = str_to_transform(transform)

tosave = []
per_camera = {camera: {} for camera in camera_names}
for label, transform in cameras.items():
    for camera in camera_names:
        if label.startswith(camera):
            tosave.append(transform)
            per_camera[camera][label[len(camera):]] = transform

save_cameras(tosave, "/tmp/agisoft.obj")
print(per_camera)
for camera in per_camera:
    for camera2 in per_camera:
        if camera != camera2:
            for trial in per_camera[camera]:
                p0 = per_camera[camera][trial]
                p1 = per_camera[camera2].get(trial)
                if p1:
                    print("{} => {} ||| {}:\n{}".format(camera, camera2, trial, p1.substract(p0)))
                else:
                    print("No corresponding pose {} => {} for trial {}".format(camera, camera2, trial))
