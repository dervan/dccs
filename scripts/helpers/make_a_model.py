import numpy as np
import sys


sys.path.append("..")
from dccs.math.extrinsic import Transformation
from dccs.presenter.model import save_cameras


ts = []
for pose_inline_str in sys.argv[1:]:
    pose_inline = np.array(pose_inline_str.split(","), dtype='f4')
    t = Transformation.from_inlined_quaternion(pose_inline)
    ts.append(t)

save_cameras(ts, "/tmp/camera.obj")
