import argparse
import logging
import sys

import matplotlib.pyplot as plt
import numpy as np

sys.path.append("..")
from dccs.math.extrinsic import Transformation

sys.path.append("..")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Script used to visualise camera trajectories")
    parser.add_argument("positions_files", nargs='*', help="Files with serialized positions.")
    parser.add_argument("--trans",
                        help="Transformations as space-separated 8D vector. Should be separated by a semicolon.")
    args = parser.parse_args()
    logging.basicConfig(level=logging.DEBUG)

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    if args.trans:
        transformations = iter([Transformation.identity()] + [Transformation.from_inlined_full(np.array(t.split(" "), dtype='f8'))
                           for t in args.trans.split(";")])
    # We want to start all plots in (almost) same time
    start_time = 0
    for filename in args.positions_files:
        with open(filename) as positions:
            startline = np.array(positions.readline().split(","), dtype='f8')
            start_time = max(start_time, startline[0])

    for filename in args.positions_files:
        xs = []
        ys = []
        zs = []
        shift = None
        if args.trans:
            try:
                transformation = next(transformations)
            except StopIteration:
                transformation = Transformation.identity()
        else:
            transformation = Transformation.identity()
        with open(filename) as positions:
            for entry in positions.readlines():
                full_entry = np.array(entry.split(","), dtype='f8')
                if full_entry[0] >= start_time:
                    pose = Transformation.from_inlined_quaternion(full_entry[13:])
                    if shift is None:
                        shift = pose
                    shifted_pose = pose.substract(shift).append(transformation).pure_translation
                    xs.append(shifted_pose[0])
                    ys.append(shifted_pose[1])
                    zs.append(shifted_pose[2])
        ax.plot(xs, ys, zs)

    plt.show()
