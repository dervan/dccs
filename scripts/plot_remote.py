import _config
from dccs import run_dccs
from dccs.agents.plotter import Plotter

caller2 = Plotter("tcp://localhost:9991/0", "./plot_logfile_9991_2")
run_dccs(caller2.container)
