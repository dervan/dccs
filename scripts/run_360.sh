#!/bin/bash
SESSION="session360"
cd ../src
tmux new -s $SESSION -d
tmux split-window -v -t $SESSION
tmux split-window -v -t $SESSION
FLAGS="-o -a 192.168.13.13 "
#tmux send-keys -t $SESSION:0.0 'python3 server.py' C-m
tmux send-keys -t $SESSION:0.0 "python3 ../bin/daemon.py 360/1 9991 $FLAGS -c tcp://192.168.13.13:9992/0,tcp://192.168.13.1:9991/0" C-m
tmux send-keys -t $SESSION:0.1 "python3 ../bin/daemon.py 360/2 9992 $FLAGS -c tcp://192.168.13.13:9991/0,tcp://192.168.13.1:9991/0" C-m
tmux send-keys -t $SESSION:0.2 "ssh pi@192.168.13.1 './run.sh'" C-m
tmux select-layout even-vertical
cd ../scripts
tmux attach -t $SESSION

