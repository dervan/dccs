import argparse

import _config
from dccs.math.camera_model import CameraData
from dccs.math.calibrators import StereoCalibrator, CameraCalibrator
import dccs.utils.images as dimg

parser = argparse.ArgumentParser(description="Script used to calibrate camera from camera_data, "
                                             + "based on config file and photos with calibration checkerboard.")
parser.add_argument("camera_name", help="Directory containing needed config for camera")
parser.add_argument("--time", "-t", help="Time of displaing single debug image", type=int, default=100)
parser.add_argument("--clean", "-c", action='store_true', help="Clean before calibration")
parser.add_argument("--photoclean", "-p", action='store_true',
                    help="Clean also photos. Have effect only with -c flag.")
parser.add_argument("--debug", "-d", action='store_true', help="Debug flag")
parser.add_argument("--stereo-name", "-s", help="Specify camera for stereo pair", required=False)

args = parser.parse_args()

cam_data = CameraData(args.camera_name)
# Just clean up calibration data and exit
if args.clean:
    cam_data.clean_calibration()
    print("Calibration cleaned!")
    if args.photoclean:
        print("Photos cleaned!")
        cam_data.clean_photos()
    exit(0)

elif args.stereo_name:
    slave_data = CameraData(args.stereo_name)
    stereo_calibrator = StereoCalibrator(cam_data, slave_data)
    stereo_calibrator.calibrate()
    stereo_calibrator.show_debug()
    exit(0)
else:
    # Calibration based on photos of calibration grid
    calibrator = CameraCalibrator(cam_data)
    calibrator.calibrate()

    if not args.debug:
        calibrator.dump()
        exit(0)

    for filename in cam_data.photos():
        photo = dimg.load_bgr(filename)
        rect = cam_data.get_base_camera().undistort(photo)
        dimg.cv_show(rect, max_width=1920, time=args.time)

    calibrator.print_debug_stats()
