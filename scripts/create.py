import argparse

import os

import sys

import shutil

import _config
import dccs.config

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Script used to create new camera")
    parser.add_argument("camera_root", help="Root for camera (in multicamera set)", default="")
    parser.add_argument("-n", "--name", help="camera name")
    parser.add_argument("-x", help="camera x resolution")
    parser.add_argument("-y", help="camera y resolution")
    parser.add_argument("-f", help="camera FPS")
    parser.add_argument("-d", help="v4l2 device id")
    parser.add_argument("-e", help="v4l2 format (fourcc)", default="MJPG")
    args = parser.parse_args()
    data_root = dccs.config.camera_data_root
    camera_root_path = os.path.join(data_root, args.camera_root)
    try:
        os.mkdir(camera_root_path)
    except FileExistsError:
        pass

    camera_path = os.path.join(camera_root_path, args.name)

    try:
        os.mkdir(camera_path)
    except FileExistsError:
        print("Error - path {} already exists!".format(camera_path), file=sys.stderr)
        exit(1)

    os.mkdir(os.path.join(camera_path, "data"))
    os.mkdir(os.path.join(camera_path, "photos"))
    os.mkdir(os.path.join(camera_path, "logs"))
    shutil.copy(os.path.join(data_root, "templates/slam_config.yaml"), camera_path)
    with open(os.path.join(camera_path, "config"), "w") as config_file:
        config_file.write("type=v4l\n")
        config_file.write("id=" + args.d + "\n")
        config_file.write("xres=" + args.x + "\n")
        config_file.write("yres=" + args.y + "\n")
        config_file.write("fps=" + args.f + "\n")
        config_file.write("format=" + args.e + "\n")


