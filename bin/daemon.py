#!/usr/bin/env python3
import argparse
import sys
import logging
from metrology.reporter import LoggerReporter

sys.path.append("..")
from dccs import create_container, run_dccs
from dccs.agents.camera_driver import CameraDriverServer

parser = argparse.ArgumentParser()
parser.add_argument("camera_name", help="Camera directory containing needed config")
parser.add_argument("port", type=int, help="Port on which daemon is started")
parser.add_argument("-a", "--address", help="Address of launched daemon.", default="0.0.0.0")
parser.add_argument("-p", "--preview", help="Enables window with stream preview", action='store_true', default=False)
parser.add_argument("-o", "--odom", help="Enables odometry", action='store_true')
parser.add_argument("-v", "--visual", help="Run odometry in visual debug mode", action='store_true')
parser.add_argument("-l", "--plot", help="Enables plot with positions", action='store_true')
parser.add_argument("-d", "--debug", help="Sets logging level to debug", action='store_true')
parser.add_argument("-c", "--cameras", help="URLs of other cameras, separated with comma.", default="")
parser.add_argument("-m", "--master", help="Address of calibration server.", default="localhost")
args = parser.parse_args()

container = create_container(args.address, args.port)


camera_driver = CameraDriverServer(container, args.camera_name, args.cameras.split(","),
                                   args.preview, args.odom, args.visual)
logging.basicConfig(level=logging.DEBUG if args.debug else logging.INFO)
reporter = LoggerReporter(logger=logging, level=logging.DEBUG, interval=1)
reporter.start()

run_dccs(container)

