import sys
from typing import TypeVar, List

import numpy as np
import numpy.random as rnd

from dccs.math.algebra import l2_dist

T = TypeVar('T')
M = TypeVar('M')


class RansacModel:
    def __init__(self, arity: int, inliner_threshold: float):
        self.model_arity = arity
        self.inliner_threshold = inliner_threshold
        self.model = None
        self.best_fit_ratio = 0

    def generator(self, data: List[T]) -> M:
        print("Generic generator called!")
        raise NotImplementedError

    def apply(self, model: M, data: T) -> float:
        print("Generic apply called!")
        raise NotImplementedError

    def measure_model_to_data_fit(self, model: M, data: List[T]) -> float:
        return sum([self.apply(model, entry) ** 2 for entry in data])

    def fit(self, data: List[T], max_iterations: int, acceptance_ratio: float = 0.8, inliners_threshold: float = None):
        best_model = None
        best_error = sys.float_info.max
        best_fitness = 0
        choices = range(len(data))
        inliners_threshold = inliners_threshold or self.inliner_threshold
        for it in range(max_iterations):
            data_subset_idx = np.int32(rnd.choice(choices, self.model_arity, replace=False))
            data_subset = [data[i] for i in data_subset_idx]
            model = self.generator(data_subset)
            inliners = []
            for entry in data:
                if self.apply(model, entry) < inliners_threshold:
                    inliners.append(entry)
            if len(inliners) > 0.5 * len(data):
                tuned_model = self.generator(inliners)
                model_error = self.measure_model_to_data_fit(tuned_model, inliners)
                if best_error > model_error:
                    best_model = tuned_model
                    best_error = model_error
                    best_fitness = len(inliners) / len(data)
                if len(inliners) > acceptance_ratio * len(data):
                    self.best_fit_ratio = best_fitness
                    return best_model
        self.model = best_model
        self.best_fit_ratio = best_fitness
        return best_model

    def get_model(self):
        return self.model


class ScaleFitterModel(RansacModel):
    """
    Single entry in this model is a line segment (two points, and corresponding DB points)
    """

    def __init__(self):
        super(ScaleFitterModel, self).__init__(2, 0.05)

    @staticmethod
    def scale(a, b):
        d1 = l2_dist(a[0], b[0]) + 0.000001
        d2 = l2_dist(a[1], b[1])
        return d2 / d1

    def generator(self, data):
        scales = [ScaleFitterModel.scale(*x) for x in data]
        # We know that median minimize l2 norm of that dataset
        return np.median(scales)

    def apply(self, s, segment):
        actual = ScaleFitterModel.scale(*segment)
        return abs(s / actual - 1)

    def measure_model_to_data_fit(self, model, data):
        err = 0
        for segment in data:
            err += self.apply(model, segment) ** 2
        return err

    def fit(self, data, max_iterations, acceptance_ratio=0.8):
        if (len(data) == 0):
            self.model = 0
        else:
            self.model = self.generator(data)


'''
K = np.array( [[2759.48,0.00000,1520.69], [0.00000,2764.16,1006.81], [0.00000,0.00000,1.00000]])
def essential(p0, p1, it):
    n = len(p1)
    ns = np.arange(n)
    pk0 = np.dot(lin.inv(K), p0.T).T
    pk1 = np.dot(lin.inv(K), p1.T).T
    bestScore = 1000000
    bestE = None
    for i in range(it):
        samples = rnd.choice(ns, 8)
        E = es.getE(8, pk0[samples], pk1[samples])
        print("No.", samples, "E:", E, sep = '\n')
        out = 0
        for pp1, pp2 in zip(p0, p1):
            if vec.ldist(np.dot(E, pp1), pp2) > 0.1:
                out+=1
        if out < bestScore:
            bestScore = out
            bestE = E
        print("Outliers: ", out)
        print()
    print("Best outliners: ", bestScore)
    print("Best E:\n", bestE)
    return bestE
'''

class RelativeFullPositionFitter(RansacModel):
    """Estimates relation between two coordinates frames. First argument is always called 'local' and another one is
    called 'remote'. Estimated relation coverts local -> remote"""

    def __init__(self, inliner_threshold=0.6):
        super(RelativeFullPositionFitter, self).__init__(4, inliner_threshold)

    def generator(self, data_subset: List[Tuple[Transformation, Transformation]]) -> Transformation:
        """Estimates model from minimal number of correspondences"""
        local_positions, remote_positions = zip(*data_subset)
        local_inlined = [pose.inline_quaternion for pose in local_positions]
        local_quats = [inlined[3:] for inlined in local_inlined]
        local_translations = [inlined[:3] for inlined in local_inlined]
        remote_inlined = [pose.inline_quaternion for pose in remote_positions]
        remote_quats = [inlined[3:] for inlined in remote_inlined]
        remote_translations = [inlined[:3] for inlined in remote_inlined]

        # Step one: estimate rotation difference
        restored_rotation = algebra.estimate_relative_rotation(local_quats, remote_quats)
        # Step two: estimate translation difference
        restored_pose = algebra.estimate_relative_position(local_translations, remote_translations, local_quats,
                                                           Quaternion(restored_rotation))
        # And return pose
        full_inlined_pose = np.zeros(7)
        full_inlined_pose[0:3] = restored_pose[0:3]
        full_inlined_pose[3:] = restored_rotation
        np.set_printoptions(precision=4)
        return Transformation.from_inlined_quaternion(full_inlined_pose, scale=restored_pose[3])

    def apply(self, model: Transformation, single_entry: Tuple[Transformation, Transformation]) -> float:
        base, relative = single_entry
        np.set_printoptions(precision=4)
        scaled_relative = relative.rescaled(model.scale)
        m = model.full
        around = Transformation.from_full(np.linalg.inv(m).dot(scaled_relative.full).dot(m))
        # print(base.inline_quaternion, " | ", around.inline_quaternion)
        real_pose_diff = around.substract(base)
        # print("=", real_pose_diff.inline_quaternion, algebra.l2_norm(real_pose_diff.inline))
        return algebra.l2_norm(real_pose_diff.inline)