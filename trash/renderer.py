import sys
import aiomas
from flow3d.renderer import RendererThread 

class RendererAgent(aiomas.Agent):
    """ Aiomas agent which serves renderer functions via network"""
    def __init__(self, container: aiomas.Container, master_address: str=None) -> None:
        super().__init__(container)
        self.renderer_thread = RendererThread()
        self.renderer_thread.start()
        self.master_proxy = None
        self.master_address = master_address

    async def connect_to_master(self):
        """Tries to connect to master."""
        try:
            print("Connecting to master...")
            self.master_proxy = await self.container.connect(self.master_address)
            await self.master_proxy.register_renderer(self)
        except:
            print("Cannot connect to master!")
            print(sys.exc_info())

    @aiomas.expose
    def add_camera(self, position_vector: np.ndarray):
        print("adding new camera in position: ", position_vector)
        self.renderer_thread.add("camera", position_vector)

    @aiomas.expose
    def add_points(self, points):
        print("adding points: ", len(points))
        self.renderer_thread.add("points", np.hstack([points, np.ones_like(points)]))

