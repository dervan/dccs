from flow3d import RendererThread, PointRenderer, WireframeRenderer

_camera_object = (np.array([[0, 0, 0],
                            [0.5, 0.5, 1],
                            [-0.5, 0.5, 1],
                            [-0.5, -0.5, 1],
                            [0.5, -0.5, 1],
                            [0, 0.6, 1]]).astype('f4'),
                  np.array([[0, 1], [0, 2], [0, 3], [0, 4],
                            [1, 2], [2, 3], [3, 4], [4, 1],
                            [1, 5], [5, 2]], dtype='int32').reshape(-1))

class RigRendererThread(RendererThread):
    def prepare(self):
        super().prepare()
        self.add_renderer("points", PointRenderer)
        self.add_renderer("camera", WireframeRenderer, _camera_object)

